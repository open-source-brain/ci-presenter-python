from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal
from modules.discrimination.discrimination_module import DiscriminationModule
from cochlear_tools.stimulus.base_stimulus_definitions import SignalPath, ElectricParameters
from cochlear_tools.streamer import Player
from cochlear_tools.stimulus.streamer_stimulus_definitions import JoinStimulus, CIStimulusMaker

from PyQt5 import QtGui
import json

subject_template = {'name': None,
                    'last_name': None,
                    'anonymous_code': None,
                    'date_of_birth': None,
                    'left_implant': None,
                    'right_implant': None}


def read_exp_file(file_name=''):
    _dict = {}
    with open(file_name) as json_data:
        _dict = json.load(json_data)
    assert 'platform' in _dict.keys()
    assert 'stimuli' in _dict.keys()
    return _dict


def get_stimuli(config_dict={}):
    out = []
    for _stim in config_dict['stimuli']:
        _ep = ElectricParameters(**_stim['electric_parameters'])
        out.append(CIStimulusMaker(electrical_parameters=_ep,
                                   function_name=_stim['function_name'],
                                   defaults=_stim['defaults'],
                                   path=_stim['path']
                                   ))
    return out


class Task(object):
    def __init__(self, **kwargs):
        self.task_message = kwargs.get('task_message', None)
        self.intervals = kwargs.get('intervals', None)
        self.reference = kwargs.get('reference', None)
        self.target = kwargs.get('target', None)
        self.target_position = kwargs.get('task_position', None)
        self.target_parameter = kwargs.get('task_parameter', None)
        self.step_size = kwargs.get('task_message', None)
        self.gap = kwargs.get('gap', None)
        self.feedback = kwargs.get('feedback', None)


class Subject(object):
    def __init__(self, **kwargs):
        self.name = kwargs.get('name', None)
        self.last_name = kwargs.get('last_name', None)
        self.anonymous_code = kwargs.get('anonymous_code', None)
        self.date_of_birth = kwargs.get('date_of_birth', None)
        self.left_implant = kwargs.get('left_implant', None)
        self.right_implant = kwargs.get('right_implant', None)

    def update(self, parameters={}):
        for _key, value in self.__dict__.items():
            for _new_key, _new_val in parameters.items():
                if _key == _new_key:
                    setattr(self,_key, _new_val)
                    break

    def is_selected(self):
        out = True
        if self.name is None:
            out = False
            msg = QtGui.QMessageBox()
            msg.setIcon(QtGui.QMessageBox.Information)
            msg.setText("A participant must be selected")
            msg.setWindowTitle("Participant warning")
            msg.setDetailedText("None participant selected")
            msg.setStandardButtons(QtGui.QMessageBox.Ok)
            msg.exec_()
        return out


class Experiment(QtCore.QObject):
    signal_mode_changed = pyqtSignal(bool)

    def __init__(self):
        super(Experiment, self).__init__()
        self.platform = None
        self.stimuli = None
        self.task = None
        self.bilateral_stimulation = False
        self.thread_play = None
        self.subject = Subject()
        self.host_app = QtGui.QApplication.instance()
        self.discrimination_module = None
        self.player = Player()
        self.signal_mode_changed.connect(self.player.on_mode_changed)

    def read_experiment(self, exp_file=''):
        config = read_exp_file(exp_file)
        self.platform = config['platform']
        self.player.platform = self.platform
        self.bilateral_stimulation = config['bilateral_stimulation']
        self.stimuli = self.read_stimuli(config['stimuli'])
        if 'discrimination_task' in config.keys():
            _dis_conf = config['discrimination_task']
            self.discrimination_module = DiscriminationModule(subject=self.subject,
                                                              stimuli=self.stimuli,
                                                              platform=self.platform,
                                                              **_dis_conf)
            self.signal_mode_changed.connect(
                self.discrimination_module.discrimination_task.player.on_mode_changed)

    @staticmethod
    def read_stimuli(stimuli={}):
        stim = []
        for _stim in stimuli:
            if _stim['function_name'] != 'SeqJoiner':
                stim.append(CIStimulusMaker(**_stim))
        for _stim in stimuli:
            if _stim['function_name'] == 'SeqJoiner':
                joined_stim = []
                for _item in _stim['defaults']['stimuli']:
                    for _normal_stim in stim:
                        if _normal_stim.label == _item:
                            joined_stim.append(_normal_stim)

                _n_epochs = _stim['defaults']['n_epochs']
                _stim['defaults'] = {'n_epochs': _n_epochs}
                stim.append(JoinStimulus(stimuli=joined_stim, **_stim))
        return stim

    def play(self, stimulus=None):
        if not self.subject.is_selected():
            return
        self.player.play(sequence=stimulus)

    def stop(self):
        self.player.stop()

    def set_subject(self, subject):
        self.subject.update(subject)
