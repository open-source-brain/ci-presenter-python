from PyQt5 import QtGui, QtCore
from PyQt5.Qt import QWidget
from cochlear_tools.streamer import Player
from cochlear_tools.stimulus.base_stimulus_definitions import BilateralSequence
from cochlear_tools.stimulus.streamer_stimulus_definitions import CustomSequence, CustomStreamerStimulus
from cochlear_tools.definitions import TimeEvent
from cochlear.nic import nic4
import datetime
import time
from os.path import sep, isdir, expanduser
from os import makedirs
import random
import json

_data_path = expanduser('~') + sep + 'ci_presenter_results'
_data_file = 'subjects_data.json'


class DiscriminationModule(QWidget):

    def __init__(self, platform=None, stimuli=None, subject=None, **kwargs):
        super(DiscriminationModule, self).__init__()
        self.stimuli = stimuli
        self.subject = subject
        self.discrimination_task = DiscriminationTask(platform=platform,
                                                      subject=subject,
                                                      **kwargs)
        self.pb_start = QtGui.QPushButton(u'Start')
        self.discrimination_window = None
        self.host_app = QtGui.QApplication.instance()

    def set_discrimination_tab(self, tabs):
        tab_2 = QtGui.QWidget()
        layout_tab_2 = QtGui.QHBoxLayout()
        options_box_t2 = QtGui.QHBoxLayout()

        frames = list()
        frames.append(self.set_discrimination_options())

        discrimination_main_frame = QtGui.QFrame()
        _main_layout = QtGui.QVBoxLayout(discrimination_main_frame)
        for _frame in frames:
            _main_layout.addWidget(_frame)
        options_box_t2.addWidget(discrimination_main_frame)

        layout_tab_2.addLayout(options_box_t2, stretch=0.2)
        tab_2.setLayout(layout_tab_2)
        tab_2.setObjectName('tab_discrimination')
        tabs.addTab(tab_2, 'Discrimination')

    def set_discrimination_options(self):
        dt = self.discrimination_task
        frame = QtGui.QFrame()
        stimuli = self.stimuli
        options_box = QtGui.QGridLayout(frame)
        options_box.addWidget(QtGui.QLabel(u'Reference'), *[0, 0])
        options_box.addWidget(QtGui.QLabel(u'Target'), *[0, 1])

        # add stimulus reference
        qcb_stimulus_reference = QtGui.QComboBox()
        for _i, _stim in enumerate(stimuli):
            qcb_stimulus_reference.addItem(_stim.label, userData=_stim)
        options_box.addWidget(qcb_stimulus_reference, *[1, 0])
        qcb_stimulus_reference.currentIndexChanged.connect(self.on_set_reference)
        # initialize reference box
        qcb_stimulus_reference.currentIndexChanged.emit(0)
        # add stimulus target
        qcb_stimulus_target = QtGui.QComboBox()
        for _i, _stim in enumerate(stimuli):
            qcb_stimulus_target.addItem(_stim.label, userData=_stim)
        options_box.addWidget(qcb_stimulus_target, *[1, 1])
        qcb_stimulus_target.currentIndexChanged.connect(self.on_set_target)
        qcb_stimulus_target.currentIndexChanged.emit(0)

        # check and set default target and reference
        for _i, _stim in enumerate(stimuli):
            if self.discrimination_task.reference_label == _stim.label:
                qcb_stimulus_reference.setCurrentIndex(_i)
            if self.discrimination_task.target_label == _stim.label:
                qcb_stimulus_target.setCurrentIndex(_i)

        # add intervals
        _validator = QtGui.QIntValidator(2, 3)
        options_box.addWidget(QtGui.QLabel(u'Number of Intervals'), *[2, 0])
        qle_number_intervals = QtGui.QLineEdit()
        qle_number_intervals.setText(str(dt.number_intervals))
        # qle_number_intervals.setValidator(_validator)
        options_box.addWidget(qle_number_intervals, *[2, 1])
        qle_number_intervals.textEdited.connect(self.on_set_number_intervals)
        qle_number_intervals.textEdited.emit(qle_number_intervals.text())

        # add trials
        _validator = QtGui.QIntValidator(1, 100000000)
        options_box.addWidget(QtGui.QLabel(u'Number of trials'), *[4, 0])
        qle_number_trials = QtGui.QLineEdit()
        qle_number_trials.setText(str(dt.number_trials))
        # qle_number_trials.setValidator(_validator)
        options_box.addWidget(qle_number_trials, *[4, 1])
        qle_number_trials.textEdited.connect(self.on_set_number_trials)
        qle_number_trials.textEdited.emit(qle_number_trials.text())

        # add inter-pulse interval
        _validator = QtGui.QIntValidator(1, 100000000)
        options_box.addWidget(QtGui.QLabel(u'Inter-stimulus interval [ms]'), *[5, 0])
        qle_isi = QtGui.QLineEdit()
        qle_isi.setText(str(dt.inter_stimulus_interval))
        # qle_isi.setValidator(_validator)
        options_box.addWidget(qle_isi, *[5, 1])
        qle_isi.textEdited.connect(self.on_set_isi)
        qle_isi.textEdited.emit(qle_isi.text())

        # add play button
        self.pb_start.clicked.connect(self.on_start_discrimination_task)
        options_box.addWidget(self.pb_start, *[6, 0])
        return frame

    def on_set_reference(self):
        _combo_box = self.sender()
        reference = _combo_box.itemData(_combo_box.currentIndex())
        self.discrimination_task.reference = reference

    def on_set_target(self):
        _combo_box = self.sender()
        target = _combo_box.itemData(_combo_box.currentIndex())

        self.discrimination_task.target = target

    def on_set_number_intervals(self):
        _edit_line = self.sender()
        n_intervals = int(_edit_line.text())
        self.discrimination_task.number_intervals = n_intervals

    def on_set_number_trials(self):
        _edit_line = self.sender()
        n_trials = int(_edit_line.text())
        self.discrimination_task.number_trials = n_trials

    def on_set_isi(self):
        _edit_line = self.sender()
        isi = float(_edit_line.text())
        self.discrimination_task.inter_stimulus_interval = isi

    def on_start_discrimination_task(self):
        if not self.subject.is_selected():
            return
        self.pb_start.setEnabled(False)
        self.discrimination_window = DiscriminationWindow(discrimination_task=self.discrimination_task)
        self.set_signals()
        self.enable_stimulus_widgets(False)
        self.discrimination_window.dialog.exec_()
        self.pb_start.setEnabled(True)

    def on_end(self):
        self.discrimination_window.pb_start.setEnabled(True)

    def set_signals(self):
        self.discrimination_task.player.signal_stream_ended.connect(self.on_stimuli_played)
        self.discrimination_task.player.signal_time_event.connect(self.on_time_event)
        self.discrimination_window.pb_option_1.clicked.connect(self.on_option_selected)
        self.discrimination_window.pb_option_2.clicked.connect(self.on_option_selected)
        self.discrimination_window.begin_button.clicked.connect(self.begin)
        self.discrimination_window.cancel_button.clicked.connect(self.cancel)

    def cancel(self):
        self.discrimination_window.dialog.close()
        self.pb_start.setEnabled(True)

    def begin(self):
        self.discrimination_window.begin_button.setEnabled(False)
        self.discrimination_window.begin_button.setStyleSheet("font-size:32px; background-color: green")
        self.host_app.processEvents()
        time.sleep(0.5)
        self.discrimination_window.begin_button.setStyleSheet("font-size:32px")
        self.host_app.processEvents()
        dt = self.discrimination_task
        dt.generate_sequences()
        dt.current_trial = 0
        dt.results = []
        dt.data_path = _data_path + sep + self.subject.anonymous_code + sep + 'discrimination'
        dt.file_name = self.subject.anonymous_code + '_' + \
                       datetime.datetime.now().strftime('%d_%b_%Y_H%H_M%M_S%S') + '.json'
        self.play_trial()

    def play_trial(self):
        self.enable_stimulus_widgets(False)
        dt = self.discrimination_task
        dt.current_sequence_left = nic4.Sequence()
        dt.current_sequence_right = nic4.Sequence()
        if dt.reference.path == "left" and dt.target.path == "left":
            dt.current_sequence_left = dt.sequences[dt.current_trial]
        if dt.reference.path == "right" and dt.target.path == "right":
            dt.current_sequence_right = dt.sequences[dt.current_trial]
        self.discrimination_task.play()

    def on_option_selected(self):
        self.enable_stimulus_widgets(False)
        _time = datetime.datetime.now().strftime('%d %b %Y: %H-%M-%S')
        _button = self.sender()
        dt = self.discrimination_task
        target_position = dt.sequences[dt.current_trial]['target_position']
        dt.current_trial += 1
        selection = int(_button.text()) - 1
        if dt.feedback:
            if selection == target_position:
                _button.setStyleSheet("font-size:32px; background-color: green")
                self.host_app.processEvents()
                time.sleep(0.3)
                _button.setStyleSheet("font-size:32px")
                self.host_app.processEvents()
            else:
                _button.setStyleSheet("font-size:32px; background-color: red")
                self.host_app.processEvents()
                time.sleep(0.3)
                _button.setStyleSheet("font-size:32px")
                self.host_app.processEvents()
        else:
            _button.setStyleSheet("font-size:32px; background-color: gray")
            self.host_app.processEvents()
            time.sleep(0.3)
            _button.setStyleSheet("font-size:32px")
            self.host_app.processEvents()

        _result = {'target_position': target_position, 'answer': selection, 'time': _time}
        dt.results.append(_result)
        dt.save_results()
        if dt.current_trial < dt.number_trials:
            self.play_trial()
        else:
            self.discrimination_window.task_text.setText('Done, well done!!')
            self.discrimination_window.task_text.setAlignment(QtCore.Qt.AlignCenter)
            self.host_app.processEvents()
            time.sleep(3.0)
            self.discrimination_window.dialog.close()

    def enable_stimulus_widgets(self, state=True):
        [_cb.setEnabled(state) for _cb in self.discrimination_window.button_selection_list]

    def on_stimuli_played(self):
        self.enable_stimulus_widgets(True)
        [_b.setStyleSheet("font-size:32px") for _b in self.discrimination_window.button_selection_list]

    def on_time_event(self, _event):
        for _i, _b in enumerate(self.discrimination_window.button_selection_list):
            if _event.index == _i:
                if _event.sent:
                    _b.setStyleSheet("font-size:32px")
                else:
                    _b.setStyleSheet("font-size:32px; background-color: gray")
            else:
                _b.setStyleSheet("")
        QtGui.QApplication.processEvents()
        print(_event)


class DiscriminationTask(QtCore.QObject):
    def __init__(self, platform=None, subject=None, **kwargs):
        super(DiscriminationTask, self).__init__()
        self.player = Player()
        self.player.platform = platform
        self.subject = subject
        self.reference = None
        self.target = None
        self.reference_label = kwargs.get('reference_label', None)
        self.target_label = kwargs.get('target_label', None)
        self.number_intervals = kwargs.get('number_intervals', 2)
        self.number_trials = kwargs.get('number_trials', 10)
        self.inter_stimulus_interval = kwargs.get('inter_stimulus_interval', 500)
        self.feedback = kwargs.get('feedback', True)
        self.task_message = kwargs.get('task_message', 'no message set')
        self.sequences = []
        self.streamer_left = None
        self.streamer_right = None
        self.current_sequence_left = None
        self.__stop_streaming = True
        self.app = QtGui.QApplication.instance()
        self._current_trial = 0
        self.results = []
        self.host_app = QtGui.QApplication.instance()
        self.data_path = None
        self.file_name = None

    def get_current_trial(self):
        return self._current_trial

    def set_current_trial(self, value):
        self._current_trial = value

    current_trial = property(get_current_trial, set_current_trial)

    def generate_sequences(self):
        self.sequences = []
        for _i in range(self.number_trials):
            _seq_left = nic4.Sequence()
            _seq_right = nic4.Sequence()
            f_length = max(self.reference.electrical_parameters.get_min_frame_length(),
                           self.target.electrical_parameters.get_min_frame_length())
            stim_null = nic4.NullStimulus(f_length)
            command_null = nic4.StimulusCommand(stim_null)
            empty_seq = nic4.Sequence(int(self.inter_stimulus_interval * 1e3 / f_length))
            empty_seq.append(command_null)
            stimuli = [self.reference, self.target]
            _idx = random.sample([0, 1], len(stimuli))
            _stim_duration = 0.0
            _event_time = []
            _stim_index = 0
            if self.number_intervals == 3:
                seq = self.reference.get_sequence()
                _ref_stim_left, _ref_stim_right = seq.get_stimulus()
                l_seq = _ref_stim_left.get_sequence()
                r_seq = _ref_stim_right.get_sequence()
                _seq_left.append(l_seq)
                _seq_right.append(r_seq)
                _stim_duration += max(l_seq.getStimulationTime_ms(),
                                      r_seq.getStimulationTime_ms())

                _time_event = TimeEvent(start=0.0,
                                        end=_stim_duration,
                                        label='reference',
                                        index=_stim_index,
                                        sent=False)
                _event_time.append(_time_event)
                _stim_duration += empty_seq.getStimulationTime_ms()
                _seq_left.append(empty_seq)
                _seq_right.append(empty_seq)
                _stim_index += 1

            for _iter, _j in enumerate(_idx):
                _seq = stimuli[_j].get_sequence()
                _ref_stim_left, _ref_stim_right = _seq.get_stimulus()
                l_seq = _ref_stim_left.get_sequence()
                r_seq = _ref_stim_right.get_sequence()
                _seq_left.append(l_seq)
                _seq_right.append(r_seq)
                _stim_duration += max(l_seq.getStimulationTime_ms(),
                                      r_seq.getStimulationTime_ms())
                _label = 'reference' if _j == 0 else 'target'
                _time_event = TimeEvent(start=_stim_duration,
                                        end=_stim_duration,
                                        label=_label,
                                        index=_stim_index,
                                        sent=False)
                _event_time.append(_time_event)
                _stim_index += 1
                if _iter < len(_idx) - 1:
                    _seq_left.append(empty_seq)
                    _seq_right.append(empty_seq)
                    _stim_duration += empty_seq.getStimulationTime_ms()
            _left_sequence = CustomSequence(ep=stimuli[_j].electrical_parameters,
                                            sequence=_seq_left)
            _right_sequence = CustomSequence(ep=stimuli[_j].electrical_parameters,
                                             sequence=_seq_right)

            _sequenced_stim = BilateralSequence(left_channel_sequence=_left_sequence,
                                                right_channel_sequence=_right_sequence,
                                                time_events=_event_time)
            _streamer_seq = CustomStreamerStimulus(sequence=_sequenced_stim)

            # self.sequences.append(_sequenced_stim)
            self.sequences.append({'sequence': _streamer_seq, 'target_position': _idx.index(1),
                                   'time_events': _event_time})

    def assign_stimuli_path(self):
        #  check that stimuli going to same path are both set to same mode
        if self.reference.path == self.target.path:
            assert(self.reference.electrical_parameters.el_reference == self.target.electrical_parameters.el_reference)

            # self.streamer_left = get_streamer(platform=self.platform,
            #                                   ep=self.reference.electrical_parameters,
            #                                   signal_path="left")
            # self.streamer_right = get_streamer(platform=self.platform,
            #                                    ep=self.reference.electrical_parameters,
            #                                    signal_path="right")
        else:
            if self.reference.path == "left":
                a=1
                # self.streamer_left = get_streamer(platform=self.platform,
                #                                   ep=self.reference.electrical_parameters,
                #                                   signal_path="left")
                # self.streamer_right = get_streamer(platform=self.platform,
                #                                    ep=self.target.electrical_parameters,
                #                                    signal_path="right")
            else:
                a = 1
                # self.streamer_left = get_streamer(platform=self.platform,
                #                                   ep=self.target.electrical_parameters,
                #                                   signal_path="left")
                # self.streamer_right = get_streamer(platform=self.platform,
                #                                    ep=self.reference.electrical_parameters,
                #                                    signal_path="right")

    def play(self):
        # self.assign_stimuli_path()
        _time_events = self.sequences[self.current_trial]['time_events']
        self.player.play(sequence=self.sequences[self.current_trial]['sequence'],
                         time_events=_time_events)

    def save_results(self):
        data_path = self.data_path
        if not isdir(data_path):
            makedirs(data_path)
        out = dict()
        out['subject'] = self.subject.__dict__
        out['task'] = self.get_parameters()
        out['reference'] = {'electrical_parameters': self.reference.electrical_parameters.get_parameters(),
                            'stimulus': self.reference.current_parameters}
        out['target'] = {'electrical_parameters': self.target.electrical_parameters.get_parameters(),
                         'stimulus': self.target.current_parameters}

        out['results'] = self.results
        _number_correct = sum([_item['target_position'] == _item['answer'] for _item in self.results])
        out['summary'] = {'number_trials': len(self.results),
                          'number_corrects': _number_correct,
                          'percentage': float(_number_correct) / float(len(self.results))}
        with open(self.data_path + sep + self.file_name, 'w') as fp:
            json.dump(out, fp, indent=1)

    def get_parameters(self):
        _params = self.__dict__
        out = {}
        for _p in _params.keys():
            if (isinstance(_params[_p], int) or isinstance(_params[_p], float) or isinstance(_params[_p], bool)) and \
                    not _p.startswith('_'):
                out[_p] = _params[_p]
        return out


class DiscriminationWindow(QWidget):
    def __init__(self, discrimination_task=DiscriminationTask):
        super(DiscriminationWindow, self).__init__()
        self.discrimination_task = discrimination_task
        dt = self.discrimination_task
        self.dialog = QtGui.QDialog()
        self.dialog.setModal(True)
        self.dialog.setWindowTitle("Task")
        self.button_selection_list = []
        self.begin_button = QtGui.QPushButton(u'Begin')
        self.cancel_button = QtGui.QPushButton(u'Cancel')
        self.pb_option_1 = QtGui.QPushButton(u'1')
        self.pb_option_2 = QtGui.QPushButton(u'2')

        main_frame = QtGui.QFrame()
        main_layout = QtGui.QVBoxLayout(main_frame)
        frame = QtGui.QFrame()
        options_box = QtGui.QGridLayout(frame)
        # task text
        _text_frame = QtGui.QFrame()
        _text_frame_layout = QtGui.QGridLayout(_text_frame)
        self.task_text = QtGui.QTextEdit()
        self.task_text.setFontPointSize(32)
        self.task_text.setText(dt.task_message)
        self.task_text.setAlignment(QtCore.Qt.AlignCenter)
        self.task_text.setReadOnly(True)
        _text_frame_layout.addWidget(self.task_text)
        options_box.addWidget(_text_frame)

        # add selection options
        _selection_buttons_frame = QtGui.QFrame()
        _selection_buttons_frame_layout = QtGui.QHBoxLayout(_selection_buttons_frame)
        if dt.number_intervals == 3:
            # add ref button
            self.pb_reference = QtGui.QPushButton(u'Reference')
            self.pb_reference.setStyleSheet("font-size:32px")
            self.pb_reference.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding)
            self.pb_reference.setEnabled(False)
            _selection_buttons_frame_layout.addWidget(self.pb_reference, 3)
            self.button_selection_list.append(self.pb_reference)

        # add option 1
        self.pb_option_1.setStyleSheet("font-size:32px")
        self.pb_option_1.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding)
        _selection_buttons_frame_layout.addWidget(self.pb_option_1, 3)
        self.button_selection_list.append(self.pb_option_1)

        # add option 2
        self.pb_option_2.setStyleSheet("font-size:32px")
        self.pb_option_2.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding)
        _selection_buttons_frame_layout.addWidget(self.pb_option_2, 3)
        options_box.addWidget(_selection_buttons_frame)
        self.button_selection_list.append(self.pb_option_2)

        # add empty frame for space
        _start_frame = QtGui.QFrame()
        # add frame
        options_box.addWidget(_start_frame)

        # add start/cancel buttons
        _start_frame = QtGui.QFrame()
        _start_frame_layout = QtGui.QGridLayout(_start_frame)
        # add start button

        self.begin_button.setStyleSheet("font-size:32px")
        _start_frame_layout.addWidget(self.begin_button, *[0, 0])

        # add play button
        self.cancel_button.setStyleSheet("font-size:32px")
        _start_frame_layout.addWidget(self.cancel_button, *[0, 1])
        # add frame
        options_box.addWidget(_start_frame)

        main_layout.addWidget(frame)
        self.dialog.setLayout(main_layout)
        # self.dialog.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.dialog.showFullScreen()
