# Install 
This is an interface to present cochlear implant stimuli using NIC 4.1 
(cochlear).
Please make sure to install python 3.7 and the python library provided by 
cochlear nic.nic4-4.1.0-py2.py3-none-any.whl

You will need to install all the requirements (see requirements.txt). 

Importantly, remember to install all binaries required by cochlear (see their 
documentation) and also add the required path to environmental variables.


# Run
Before running any experiment, always check your stimuli in advance!

To run, add pygenerator to your python path and then run ci_app.py
Some examples are provided in cochlear_tools/experiments

Psychophysical results will be saved on your home directory/ci_presenter_results

# Getting started

- To start, you can import an experiment using the **Experiment** menu from "cochlear_tools/experiments/" folder.
- Set a new test participant
- Set stimuli parameters
- If you are using the pod, set "off line" mode off (see below)
- Play stimuli

# Off line mode
In order to test your stimuli, you can set the "Off line" mode. 

![](./screenshots/stimuli_interface.png)

If you want to plot the stimuli you can set "Plot implant output". This will generate a new plot every time you play an stimuli.

![](./screenshots/stimuli_interface_plotting_output.png)

# Screenshots

![](./screenshots/discrimination_settings.png)

![](./screenshots/discrimination_running.png)


 