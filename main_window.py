from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import QSettings
import sys
import numpy as np
from os.path import expanduser, sep
from control.control_definitions import Experiment, subject_template
from cochlear_tools.definitions import get_implant_cic
from functools import partial
from prettytable import PrettyTable, MSWORD_FRIENDLY
from data_storage.storage_tools import get_all_subject_data, EditSubjectData
from cochlear_tools import streamer


class EmittingStream(QtCore.QObject):

    textWritten = QtCore.pyqtSignal(str)

    def write(self, text):
        self.textWritten.emit(str(text))


class QNumpyLineEdit(QtGui.QLineEdit):
    textModified = QtCore.pyqtSignal(str, str)  # (before, after)

    def __init__(self, contents='', parent=None, n_max=None, n_min=None):
        super(QNumpyLineEdit, self).__init__(contents, parent)
        self.editingFinished.connect(self.__handleEditingFinished)
        self.textChanged.connect(self.__handleTextChanged)
        self._before = contents
        self.n_max = n_max
        self.n_min = n_min

    def __handleTextChanged(self, text):
        if not self.hasFocus():
            self._before = text

    def __handleEditingFinished(self):
        before, after = self._before, self.text()
        if before != after:
            try:
                _vector = np.fromstring(str(after), dtype=np.float, sep=',')
                if not _vector.any():
                    self.setText(self._before)
                    return
                if self.n_max is not None and _vector.size > self.n_max:
                    self.setText(self._before)
                    return
                if self.n_min is not None and _vector.size < self.n_min:
                    self.setText(self._before)
                    return
                after = ",".join(['{:f}'.format(_v) for _v in _vector])
                self.setText(after)
                self._before = after
                self.textModified.emit(before, after)
            except Exception as e:
                print(str(e))

    def get_array(self):
        return np.fromstring(str(self._before), dtype=np.float, sep=',')


class GuiMainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(GuiMainWindow, self).__init__()
        self.experiment = Experiment()
        self.init_ui()
        self.gui_restore_settings()
        self.left_frame = None
        self.right_frame = None
        self.stimuli_text = QtGui.QTextEdit()
        self.qcb_subjects = QtGui.QComboBox()
        self.text_current_subject = None
        self.path_cb_list = []
        self.play_button_list = []
        self.stop_button_list = []
        self.current_param_button_list = []
        self.electrical_parameters_combo_box_list = []
        self.stimulus_parameters_combo_box_list = []
        self.current_electrical_parameters_edit_list = []
        self.step_size_edit_list = []
        
        # detect when debugging app
        if sys.gettrace() is None:
            sys.stdout = EmittingStream(textWritten=self.normal_output_written)

    def __del__(self):
        # Restore sys.stdout
        sys.stdout = sys.__stdout__

    def closeEvent(self, evnt):
        print("bye")
        QtGui.QApplication.quit()

    def normal_output_written(self, text):
        """Append text to the QTextEdit."""
        # Maybe QTextEdit.append() works as well, but this is how I do it:
        text_edit = self.findChild(QtGui.QTextEdit, 'console')
        cursor = text_edit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText(text)
        text_edit.setTextCursor(cursor)
        text_edit.ensureCursorVisible()

    def init_ui(self):
        exit_action = QtGui.QAction(QtGui.QIcon(''), '&Exit', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit application')
        exit_action.triggered.connect(self.closeEvent)
        menu_bar = self.menuBar()
        exit_menu = menu_bar.addMenu('&Exit')
        exit_menu.addAction(exit_action)

        settings_menu = menu_bar.addMenu('&Experiment')
        load_settings_action = QtGui.QAction(QtGui.QIcon(''), '&Read Experiment', self)
        load_settings_action.setShortcut('Ctrl+E')
        load_settings_action.setStatusTip('Read Experiment')
        load_settings_action.triggered.connect(lambda: self.gui_load_experiment_from_file())
        settings_menu.addAction(load_settings_action)

        mode_menu = menu_bar.addMenu('&Mode')
        change_mode_action = QtGui.QAction(QtGui.QIcon(''), '&Off line', self)
        change_mode_action.setCheckable(True)
        change_mode_action.setShortcut('Ctrl+O')
        change_mode_action.setStatusTip('Set mode to offline to allow working without Cochlear hardware')
        change_mode_action.triggered.connect(lambda: self.on_set_mode())
        mode_menu.addAction(change_mode_action)

        plot_stimulation_action = QtGui.QAction(QtGui.QIcon(''), '&Plot implant output', self)
        plot_stimulation_action.setCheckable(True)
        plot_stimulation_action.setShortcut('Ctrl+P')
        plot_stimulation_action.setStatusTip('If selected, implant output will be plot')
        plot_stimulation_action.triggered.connect(lambda: self.on_set_plot())
        mode_menu.addAction(plot_stimulation_action)

        help_action = QtGui.QAction(QtGui.QIcon(''), '&Help', self)
        help_action.setShortcut('Ctrl+H')
        help_action.setStatusTip('Help about application')
        help_action.triggered.connect(self.about_dialog)
        help_menu = menu_bar.addMenu('&Help')
        help_menu.addAction(help_action)

        self.statusBar().showMessage('Ready')
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Streaming Interface')
        self.tabs = QtGui.QTabWidget()
        self.show()

    def set_in_data_tab(self, tabs):
        tab_1 = QtGui.QWidget()
        main_frame = QtGui.QFrame()
        main_layout = QtGui.QVBoxLayout(main_frame)
        # set left options frame
        frames = list()
        frames.append(self.subject_frame())
        frames.append(self.set_stimuli_frame())
        # frames.append(self.set_start_stop_frame())
        frames.append(self.set_messages_frame())

        self.left_frame = QtGui.QFrame()
        left_f_layout = QtGui.QVBoxLayout(self.left_frame)
        for _frame in frames:
            left_f_layout.addWidget(_frame)

        # add splits between frames
        splitter = QtGui.QSplitter()
        splitter.addWidget(self.left_frame)

        self.right_frame = QtGui.QFrame()
        right_f_layout = QtGui.QVBoxLayout(self.right_frame)
        right_f_layout.addWidget(self.set_stimuli_text_frame())
        splitter.addWidget(self.right_frame)
        main_layout.addWidget(splitter)

        tab_1.setLayout(main_layout)
        tab_1.setObjectName('tab_settings')
        tabs.addTab(tab_1, 'CI-Presentation')
        self.tabs.show()

    def subject_frame(self):
        frame = QtGui.QFrame()
        options_box = QtGui.QGridLayout(frame)
        options_box.addWidget(QtGui.QLabel(u'Subject'), *[0, 0])

        self.qcb_subjects.setObjectName('cb_subjects')
        options_box.addWidget(self.qcb_subjects, *[0, 1])
        self.update_subjects()
        # add new button
        pb_new = QtGui.QPushButton(u'New / Edit')
        pb_new.setObjectName('pb_new_edit')
        pb_new.clicked.connect(self.add_subject)
        options_box.addWidget(pb_new, *[0, 2])

        # add current info text box
        self.text_current_subject = QtGui.QTextEdit()
        self.text_current_subject.setReadOnly(True)
        self.text_current_subject.setAcceptRichText(True)
        options_box.addWidget(self.text_current_subject, *[1, 0])
        return frame

    def set_stimuli_frame(self):
        frame = QtGui.QFrame()
        stimuli = self.experiment.stimuli
        options_box = QtGui.QGridLayout(frame)
        options_box.addWidget(QtGui.QLabel(u'Stimulus Label'), *[0, 0])
        options_box.addWidget(QtGui.QLabel(u'Electric Parameters'), *[0, 1])
        options_box.addWidget(QtGui.QLabel(u'Current Value'), *[0, 2])
        options_box.addWidget(QtGui.QLabel(u'Stimulus Parameters'), *[0, 3])
        options_box.addWidget(QtGui.QLabel(u'Current Value'), *[0, 4])
        options_box.addWidget(QtGui.QLabel(u'Step Size'), *[0, 5])
        options_box.addWidget(QtGui.QLabel(u'Direction'), *[0, 6])
        self.path_cb_list = []
        self.play_button_list = []
        self.stop_button_list = []
        self.current_param_button_list = []
        self.electrical_parameters_combo_box_list = []
        self.stimulus_parameters_combo_box_list = []
        self.current_electrical_parameters_edit_list = []
        self.step_size_edit_list = []
        _ini_pos = 1
        for _i, _stim in enumerate(stimuli):
            options_box.addWidget(QtGui.QLabel(_stim.label), *[_ini_pos + _i, 0])

            _validator = QtGui.QDoubleValidator()
            # add electrical parameters
            if _stim.type != 'SeqJoiner':
                qcb_ep = QtGui.QComboBox()
                ep = _stim.electrical_parameters.get_parameters()
                [qcb_ep.addItem(_key, userData=_stim) for _key in ep.keys()]
                qcb_ep.setObjectName('cb_ep_' + _stim.label)
                options_box.addWidget(qcb_ep, *[_ini_pos + _i, 1])
                self.electrical_parameters_combo_box_list.append(qcb_ep)

                # current electrical parameter
                qle_current_param = QtGui.QLineEdit()
                qle_current_param.setText('0')
                qle_current_param.setValidator(_validator)
                qle_current_param.setObjectName('ql_ep_' + _stim.label)
                options_box.addWidget(qle_current_param, *[_ini_pos + _i, 2])
                self.current_electrical_parameters_edit_list.append(qle_current_param)

                # connect ep combo box
                qcb_ep.currentIndexChanged.connect(partial(self.on_current_ep_param_switched, qle_current_param))
                # connect ep edit text
                qle_current_param.editingFinished.connect(partial(self.on_current_ep_param_updated, qcb_ep))

                # ep initialize values
                qcb_ep.currentIndexChanged.emit(0)

                # add direction of sounds
                qcb_path = QtGui.QComboBox()
                qcb_path.addItem('none', userData=_stim)
                qcb_path.addItem('left', userData=_stim)
                qcb_path.addItem('right', userData=_stim)
                options_box.addWidget(qcb_path, *[_ini_pos + _i, 6])
                # connect direction combo box
                qcb_path.currentIndexChanged.connect(self.on_path_changed)
                self.path_cb_list.append(qcb_path)
                # emit signal to initialize items
                qcb_path.currentIndexChanged.emit(0)

            # add stimulus parameters
            qcb_stimulus_parameters = QtGui.QComboBox()
            _parameters = _stim.current_parameters
            [qcb_stimulus_parameters.addItem(_key, userData=_stim) for _key in _parameters.keys()]
            qcb_stimulus_parameters.setObjectName('cb_' + _stim.label)
            options_box.addWidget(qcb_stimulus_parameters, *[_ini_pos + _i, 3])
            self.stimulus_parameters_combo_box_list.append(qcb_stimulus_parameters)

            # add current parameter value value
            qle_current_param = QtGui.QDoubleSpinBox(decimals=4)
            qle_current_param.setMinimum(-np.Inf)
            qle_current_param.setMaximum(np.Inf)
            qle_current_param.setValue(0)
            qle_current_param.setObjectName('ql_' + _stim.label)
            options_box.addWidget(qle_current_param, *[_ini_pos + _i, 4])
            self.current_param_button_list.append(qle_current_param)

            # connect combo box
            qcb_stimulus_parameters.currentIndexChanged.connect(partial(self.on_current_param_switched, qle_current_param))
            # connect edit text
            qle_current_param.valueChanged.connect(partial(self.on_current_param_updated, qcb_stimulus_parameters))

            # add step size box
            qsb_step_size = QtGui.QLineEdit()
            qsb_step_size.setText('1')
            qsb_step_size.setValidator(_validator)
            qsb_step_size.setObjectName('ql_' + _stim.label)
            options_box.addWidget(qsb_step_size, *[_ini_pos + _i, 5])
            qsb_step_size.editingFinished.connect(partial(self.on_step_size_changed, qle_current_param))
            self.step_size_edit_list.append(qsb_step_size)

            # add play button
            pb_play = QtGui.QPushButton(u'Play')
            pb_play.setObjectName('pb_play_'.join(_stim.label))
            pb_play.clicked.connect(partial(self.play, _stim))
            options_box.addWidget(pb_play, *[_ini_pos + _i, 7])
            self.play_button_list.append(pb_play)

            # add stop button
            pb_stop = QtGui.QPushButton(u'Stop')
            pb_stop.setObjectName('pb_stop_'.join(_stim.label))
            pb_stop.clicked.connect(self.stop)
            options_box.addWidget(pb_stop, *[_ini_pos + _i, 8])
            self.stop_button_list.append(pb_stop)

            # call event to initialize values
            qcb_stimulus_parameters.currentIndexChanged.emit(0)
            qsb_step_size.textEdited.emit(qsb_step_size.text())

        return frame

    def set_start_stop_frame(self):
        frame = QtGui.QFrame()
        # options_box = QtGui.QHBoxLayout(frame)
        # pb_start = QtGui.QPushButton('Start')
        # pb_start.setObjectName('pb_start')
        # pb_start.clicked.connect(lambda: self.gui_save_settings())
        # options_box.addWidget(pb_start)
        # pb_stop = QtGui.QPushButton('Stop')
        # pb_stop.setObjectName('pb_stop')
        # options_box.addWidget(pb_stop)
        return frame

    @staticmethod
    def set_messages_frame():
        frame = QtGui.QFrame()
        options_box_t1_4 = QtGui.QVBoxLayout(frame)
        text_edit = QtGui.QTextEdit()
        text_edit.setObjectName('console')
        options_box_t1_4.addWidget(text_edit)
        return frame

    def set_stimuli_text_frame(self):
        frame = QtGui.QFrame()
        options_box_t1_4 = QtGui.QVBoxLayout(frame)

        self.stimuli_text.setObjectName('current_stimuli')
        options_box_t1_4.addWidget(self.stimuli_text)
        return frame

    def on_current_ep_param_switched(self, _line_edit):
        _combo_box = self.sender()
        stimulus = _combo_box.itemData(_combo_box.currentIndex())
        _key = str(_combo_box.itemText(_combo_box.currentIndex()))
        _value = stimulus.electrical_parameters.get_parameters()[_key]
        _line_edit.setText(str(_value))
        self.update_stimuli_list()

    def on_current_ep_param_updated(self, _combo_box):
        _line_edit = self.sender()
        stimulus = _combo_box.itemData(_combo_box.currentIndex())
        _key = str(_combo_box.itemText(_combo_box.currentIndex()))
        _value = float(_line_edit.text())
        _ep_parameters = stimulus.electrical_parameters.get_parameters()
        _ep_parameters[_key] = _value
        stimulus.electrical_parameters = _ep_parameters
        stimulus.get_sequence(stimulus.current_parameters)
        self.update_stimuli_list()

    def on_step_size_changed(self, _spin_box):
        _step_size_eb = self.sender()
        self.__on_step_size_changed(_step_size_eb, _spin_box)

    @staticmethod
    def __on_step_size_changed(_step_size_eb, _spin_box):
        _spin_box.setSingleStep(float(_step_size_eb.text()))

    def on_current_param_switched(self, _spin_edit):
        _combo_box = self.sender()
        self.__on_current_param_switched(_combo_box, _spin_edit)

    def __on_current_param_switched(self, _combo_box, _spin_edit):
        stimulus = _combo_box.itemData(_combo_box.currentIndex())
        _key = str(_combo_box.itemText(_combo_box.currentIndex()))
        _value = stimulus.current_parameters[_key]
        _spin_edit.setValue(_value)
        self.update_stimuli_list()

    def on_current_param_updated(self, _combo_box):
        _spin_box = self.sender()
        self.__on_current_param_updated(_spin_box, _combo_box)

    def __on_current_param_updated(self, _spin_box, _combo_box):
        stimulus = _combo_box.itemData(_combo_box.currentIndex())
        _key = str(_combo_box.itemText(_combo_box.currentIndex()))
        _value = float(_spin_box.text())
        updated_parameter = {_key: _value}
        stimulus.get_sequence(updated_parameter)
        _spin_box.setValue(stimulus.current_parameters[_key])
        self.update_stimuli_list()

    def update_stimuli_list(self):
        self.stimuli_text.setText('')
        cursor = self.stimuli_text.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        for _stim in self.experiment.stimuli:
            t = PrettyTable(['Electrical parameter', 'Current Value'])
            for key, val in _stim.electrical_parameters.get_parameters().items():
                t.add_row([key, val])
            cursor.insertText(t.get_string() + '\n')
            t = PrettyTable(['Stimulus parameter', 'Current Value'])
            for key, val in _stim.current_parameters.items():
                t.add_row([key, val])
            cursor.insertText(t.get_string() + '\n')
        # self.stimuli_text.setTextCursor(cursor)
        self.stimuli_text.ensureCursorVisible()

    def get_current_settings(self):
        # read host configuration
        host = self.findChild(QtGui.QLineEdit, 'q_line_edit_host')
        current_settings = {'host': host.text()}
        return current_settings

    def gui_save_settings(self, _settings=None):
        if _settings is None:
            _settings = QSettings(expanduser('~') + sep + '.ci_presenter' + sep + 'settings.ini', QSettings.IniFormat)
        _settings.setFallbacksEnabled(False)
        _settings.setValue('size', self.size())
        _settings.setValue('pos', self.pos())

        for obj in self.findChildren(QtGui.QWidget):
            name = obj.objectName()
            if not name:
                continue
            if isinstance(obj, QtGui.QComboBox):
                index = obj.currentIndex()
                text = obj.itemText(index)
                _settings.setValue(name, text)

            if isinstance(obj, QtGui.QLineEdit):
                value = obj.text()
                _settings.setValue(name, value)

            if isinstance(obj, QtGui.QSpinBox):
                value = obj.value()
                _settings.setValue(name, value)

            if isinstance(obj, QtGui.QDoubleSpinBox):
                value = obj.value()
                _settings.setValue(name, value)

            if isinstance(obj, QtGui.QCheckBox):
                state = obj.isChecked()
                _settings.setValue(name, state)

            if isinstance(obj, QtGui.QRadioButton):
                value = obj.isChecked()
                _settings.setValue(name, value)

            if isinstance(obj, QtGui.QListWidget):
                _settings.beginGroup(name)
                for idx_n in range(obj.count()):
                    _settings.setValue(obj.item(idx_n).text(), obj.item(idx_n).isSelected())
                _settings.endGroup()

    def gui_save_settings_to_file(self):
        dialog = QtGui.QFileDialog()
        dialog.setNameFilter('*.ini')
        dialog.setDefaultSuffix('ini')
        dialog.setAcceptMode(QtGui.QFileDialog.AcceptSave)
        if dialog.exec_():
            _file = str(dialog.selectedFiles()[0])
            _settings = QSettings(_file, QSettings.IniFormat)
            self.gui_save_settings(_settings)

    def gui_load_settings_from_file(self):
        dialog = QtGui.QFileDialog()
        dialog.setNameFilter('*.ini')
        dialog.setDefaultSuffix('ini')
        dialog.setAcceptMode(QtGui.QFileDialog.AcceptOpen)
        if dialog.exec_():
            _file = str(dialog.selectedFiles()[0])
            _settings = QSettings(_file, QSettings.IniFormat)
            self.gui_restore_settings(_settings)

    def gui_restore_settings(self, _settings=None):
        if _settings is None:
            _settings = QSettings(expanduser('~') + sep + '.ci_presenter' + sep + 'settings.ini', QSettings.IniFormat)
        _settings.setFallbacksEnabled(False)

        # Restore geometry
        try:
            self.resize(_settings.value('size', QtCore.QSize(500, 500)))
            self.move(_settings.value('pos', QtCore.QPoint(60, 60)))

            for obj in self.findChildren(QtGui.QWidget):
                name = obj.objectName()
                if isinstance(obj, QtGui.QComboBox):
                    value = (_settings.value(name))
                    if not value:
                        continue
                    index = obj.findText(value.toString())
                    if index != -1:
                        obj.setCurrentIndex(index)

                if isinstance(obj, QtGui.QLineEdit):
                    value = _settings.value(name).toString()
                    if value:
                        obj.setText(value)

                if isinstance(obj, QtGui.QSpinBox):
                    value = _settings.value(name).toInt()[0]
                    if value:
                        obj.setValue(value)

                if isinstance(obj, QtGui.QDoubleSpinBox):
                    value = _settings.value(name).toFloat()[0]
                    if value:
                        obj.setValue(value)

                if isinstance(obj, QtGui.QCheckBox):
                    value = _settings.value(name)
                    if value is not None:
                        obj.setChecked(value.toBool())

                if isinstance(obj, QtGui.QRadioButton):
                    value = _settings.value(name)
                    if value is not None:
                        obj.setChecked(value.toBool())
                if isinstance(obj, QtGui.QListWidget):
                    _c_list = self.findChild(QtGui.QListWidget, name)
                    if _c_list:
                        _settings.beginGroup(name)
                        saved_keys = _settings.childKeys()
                        for _i in range(_c_list.count()):
                            for _saved in saved_keys:
                                _item_val = _settings.value(_saved)
                                if _c_list.item(_i).text() == _saved:
                                    _c_list.item(_i).setSelected(_item_val.toBool())
                                    break
                        _settings.endGroup()
        except Exception as e:
            print(str(e))

    @staticmethod
    def about_dialog():
        d = QtGui.QDialog()
        d.setWindowTitle("About")
        text = QtGui.QTextEdit()
        text.setText('Copyright (C) 2017  Jaime Undurraga \n '
                     'This program is free software; you can redistribute it and/or '
                     'modify it under the terms of the GNU General Public License '
                     'as published by the Free Software Foundation; either version 2 '
                     'of the License, or (at your option) any later version. \n '
                     'This program is distributed in the hope that it will be useful, '
                     'but WITHOUT ANY WARRANTY; without even the implied warranty of '
                     'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the '
                     'GNU General Public License for more details. \n '
                     'You should have received a copy of the GNU General Public License '
                     'along with this program; if not, write to the Free Software '
                     'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.')
        text.setReadOnly(True)
        d.layout = QtGui.QGridLayout(d)
        d.layout.addWidget(text, 0, 0, 1, 1)
        d.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        d.exec_()

    def gui_load_experiment_from_file(self):
        dialog = QtGui.QFileDialog()
        dialog.setNameFilter('*.json')
        dialog.setDefaultSuffix('json')
        dialog.setAcceptMode(QtGui.QFileDialog.AcceptOpen)
        if dialog.exec_():
            _file = str(dialog.selectedFiles()[0])
            self.experiment.read_experiment(_file)
        self.set_in_data_tab(self.tabs)
        if self.experiment.discrimination_module is not None:
            self.experiment.discrimination_module.set_discrimination_tab(self.tabs)
        self.setCentralWidget(self.tabs)
        self.tabs.adjustSize()
        self.tabs.show()

    def play(self, stimulus):
        self.enable_stimulus_widgets(False)
        self.experiment.play(stimulus)
        self.enable_stimulus_widgets(True)

    def stop(self):
        self.experiment.stop()
        self.enable_stimulus_widgets(True)

    def add_subject(self):
        if self.experiment.subject.name is not None or self.experiment.subject.name is not None:
            EditSubjectData(self.experiment.subject.__dict__)
        else:
            EditSubjectData()
        self.update_subjects()

    def set_subject(self):
        _subject_box = self.sender()
        subject_info = _subject_box.itemData(_subject_box.currentIndex())
        if subject_info is not None:
            self.experiment.set_subject(subject_info.to_dict())
        else:
            self.experiment.set_subject(subject_template)

        t = PrettyTable([f for f in self.experiment.subject.__dict__.keys()], caching=False)
        _row = []
        for _, _val in self.experiment.subject.__dict__.items():
            _row.append(_val)
        t.add_row(_row)
        t.set_style(MSWORD_FRIENDLY)
        self.text_current_subject.setText(t.get_string())
        # update paths
        [_qcb_dir.currentIndexChanged.emit(_qcb_dir.currentIndex()) for _qcb_dir in self.path_cb_list]

    def update_subjects(self):
        self.qcb_subjects.clear()
        self.qcb_subjects.addItem('None', userData=None)
        data = get_all_subject_data()
        for _, _row in data.iterrows():
            self.qcb_subjects.addItem('{:s} {:s}'.format(_row['name'], _row['last_name']), userData=_row)
        self.qcb_subjects.currentIndexChanged.connect(self.set_subject)

    def on_path_changed(self):
        _cb_path = self.sender()
        stimulus = _cb_path.itemData(_cb_path.currentIndex())
        _selected_path = _cb_path.currentText()
        if _selected_path == 'left':
            _cic = get_implant_cic(self.experiment.subject.left_implant)
            stimulus.electrical_parameters.implant = _cic

        if _selected_path == 'right':
            _cic = get_implant_cic(self.experiment.subject.right_implant)
            stimulus.electrical_parameters.implant = _cic
        stimulus.path = _selected_path
        self.update_stimuli_list()

    def enable_stimulus_widgets(self, state=True):
        [_cb.setEnabled(state) for _cb in self.electrical_parameters_combo_box_list]
        [_cb.setEnabled(state) for _cb in self.current_electrical_parameters_edit_list]
        [_cb.setEnabled(state) for _cb in self.stimulus_parameters_combo_box_list]
        [_cb.setEnabled(state) for _cb in self.current_param_button_list]
        [_cb.setEnabled(state) for _cb in self.step_size_edit_list]
        [_cb.setEnabled(state) for _cb in self.path_cb_list]
        [_cb.setEnabled(state) for _cb in self.play_button_list]

    def on_set_mode(self):
        _status = self.sender().isChecked()
        streamer.OFF_LINE = _status

    def on_set_plot(self):
        _status = self.sender().isChecked()
        streamer.GENERATE_PLOTS = _status
