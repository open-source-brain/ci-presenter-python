import main_window as mw
from PyQt5 import QtGui
from PyQt5.QtCore import QObject
from PyQt5 import QtCore
import sys
__author__ = 'jaime undurraga'


class UserInterface(QObject):
    def __init__(self):
        QtCore.QObject.__init__(self)
        self.main_window = mw.GuiMainWindow()
        sys.exit(app.exec_())

    def initialize(self):
        pb_start = self.main_window.findChild(QtGui.QPushButton, 'pb_start')
        pb_start.clicked.connect(self.start)

        pb_stop = self.main_window.findChild(QtGui.QPushButton, 'pb_stop')
        pb_stop.clicked.connect(self.stop)

    def send_offset(self):
        sb_offset = self.main_window.findChild(QtGui.QLineEdit, 'qt_spin_box_channel_offset')
        self.bio_reader.update_offset(float(sb_offset.text()))

    def start(self):
        settings = self.main_window.get_current_settings()
        text_edit = self.main_window.findChild(QtGui.QTextEdit, 'console')
        text_edit.clear()
        self.bio_reader.start(settings)

    def stop(self):
        self.bio_reader.stop()


def close_main_window():
    print("bye")


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    app.setStyle(QtGui.QStyleFactory.create('Fusion'))
    app.setStyleSheet('QWidget{border: 1px solid gray; background-color: black; color: white}')
    ui = UserInterface()