import pandas as pd
from os.path import sep, isfile, isdir, expanduser
from os import mkdir
from PyQt5 import QtGui, QtCore
from cochlear_tools.definitions import ImplantModels
from control.control_definitions import subject_template
import inspect

_data_path = expanduser('~') + sep + '.ci_presenter'
_data_file = 'subjects_data.json'


def read_subject_data_frame(data_path=''):
    df = pd.DataFrame()
    if isfile(data_path):
        df = pd.read_json(data_path)
    return df


def get_class_attributes(input_object):
    attributes = inspect.getmembers(input_object, lambda a: not (inspect.isroutine(a)))
    out_dict ={}
    for a in attributes:
        if not (a[0].startswith('__') and a[0].endswith('__')):
            out_dict[a[0]] = a[1]
    return out_dict


class EditSubjectData(object):
    def __init__(self, subject={}):
        self.data_path = _data_path
        self.data_file_path = self.data_path + sep + _data_file
        self.data = get_all_subject_data()
        _temp_subject = subject_template.copy()
        [_temp_subject.update({_key: ''}) for _key in _temp_subject.keys()]
        self.idx_current_subject = None
        if subject:
            _row = self.data.loc[(self.data['name'] == subject['name']) & (self.data['last_name'] == subject['last_name'])]
            self.idx_current_subject = _row.index[0]
            for _key, _val in subject.items():
                _temp_subject[_key] = _val
        self.dialog = QtGui.QDialog()
        self.dialog.setWindowTitle("Subject information")
        main_frame = QtGui.QFrame()
        main_layout = QtGui.QVBoxLayout(main_frame)

        frame = QtGui.QFrame()
        options_box = QtGui.QGridLayout(frame)
        # name
        options_box.addWidget(QtGui.QLabel(u'Name'), *[0, 0])
        self.qsb_name = QtGui.QLineEdit()
        self.qsb_name.setText(_temp_subject['name'])
        self.qsb_name.setObjectName('ql_name')
        options_box.addWidget(self.qsb_name, *[0, 1])

        # last name
        options_box.addWidget(QtGui.QLabel(u'Last Name'), *[1, 0])
        self.qsb_last_name = QtGui.QLineEdit()
        self.qsb_last_name.setText(_temp_subject['last_name'])
        self.qsb_last_name.setObjectName('ql_last_name')
        options_box.addWidget(self.qsb_last_name , *[1, 1])

        # subject code
        options_box.addWidget(QtGui.QLabel(u'Anonymous Code'), *[2, 0])
        self.qsb_code = QtGui.QLineEdit()
        self.qsb_code.setText(_temp_subject['anonymous_code'])
        self.qsb_code.setObjectName('ql_anonymous_code')
        options_box.addWidget(self.qsb_code, *[2, 1])

        # date of birth
        options_box.addWidget(QtGui.QLabel(u'Date of birth'), *[3, 0])
        self.qsb_date = QtGui.QDateEdit()
        self.qsb_date.setDate(QtCore.QDate.fromString(_temp_subject['date_of_birth']))
        self.qsb_date.setObjectName('ql_date_of_birth')
        options_box.addWidget(self.qsb_date, *[3, 1])

        # left implant
        implants = get_class_attributes(ImplantModels)

        options_box.addWidget(QtGui.QLabel(u'Left Implant'), *[4, 0])
        self.qcb_left_implant = QtGui.QComboBox()
        self.qcb_left_implant.addItem('None')
        [self.qcb_left_implant.addItem(_key) for _key in implants.keys()]
        self.qcb_left_implant.setObjectName('cb_left_implant')
        index = self.qcb_left_implant.findText(_temp_subject['left_implant'], QtCore.Qt.MatchFixedString)
        if index >= 0:
            self.qcb_left_implant.setCurrentIndex(index)
        options_box.addWidget(self.qcb_left_implant, *[4, 1])

        # rigth implant
        options_box.addWidget(QtGui.QLabel(u'Right Implant'), *[5, 0])
        self.qcb_right_implant = QtGui.QComboBox()
        self.qcb_right_implant.addItem('None')
        [self.qcb_right_implant.addItem(_key) for _key in implants.keys()]
        self.qcb_right_implant.setObjectName('cb_left_implant')
        index = self.qcb_right_implant.findText(_temp_subject['right_implant'], QtCore.Qt.MatchFixedString)
        if index >= 0:
            self.qcb_right_implant.setCurrentIndex(index)
        options_box.addWidget(self.qcb_right_implant, *[5, 1])

        # add play button
        pb_start = QtGui.QPushButton(u'Accept')
        pb_start.setObjectName('pb_accept')
        pb_start.clicked.connect(self.accept)
        options_box.addWidget(pb_start, *[6, 0])

        # add play button
        pb_cancel = QtGui.QPushButton(u'Cancel')
        pb_cancel.setObjectName('pb_cancel')
        pb_cancel.clicked.connect(self.cancel)
        options_box.addWidget(pb_cancel, *[6, 1])

        main_layout.addWidget(frame)
        self.dialog.setLayout(main_layout)
        self.dialog.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.dialog.exec_()
    
    def cancel(self):
        self.dialog.close()

    def accept(self):
        data = subject_template.copy()
        data['name'] = str(self.qsb_name.text())
        data['last_name'] = str(self.qsb_last_name.text())
        data['anonymous_code'] = str(self.qsb_code.text())
        data['date_of_birth'] = str(self.qsb_date.text())
        data['left_implant'] = str(self.qcb_left_implant.currentText())
        data['right_implant'] = str(self.qcb_right_implant.currentText())
        if not self.data.size:
            self.data = pd.DataFrame(data,  index=[0])
        else:
            if self.idx_current_subject is not None:
                _subset = self.data.iloc[[self.idx_current_subject]]
                for _, _row in _subset.iterrows():
                    for _key, _val in data.items():
                        _row[_key] = _val
                self.data.iloc[self.idx_current_subject] = _row
            else:
                self.data = self.data.append(data, ignore_index=True)
        self.data.to_json(self.data_file_path)
        self.dialog.close()
        

def get_all_subject_data():
    data_path = _data_path
    data_file_path = data_path + sep + _data_file
    if not isdir (data_path):
        mkdir(data_path)
    data = pd.DataFrame()
    if isfile(data_file_path):
        data = pd.read_json(data_file_path)
    return data
