

class RateProtocol(object):
    SR5 = '25.0'
    SR4 = '20.0'
    HR5 = '12.0'
    HR4 = '9.6'


class ImplantModels(object):
    CI24M = 'CI24M'
    CI24R = 'CI24R'
    CI24RE = 'CI24RE'
    CI500 = 'CI500'


class ImplantCIC(object):
    CIC3 = 'CIC3'
    CIC4 = 'CIC4'


def get_implant_cic(implant_model=''):
    if implant_model in [ImplantModels.CI24M, ImplantModels.CI24R]:
        return ImplantCIC.CIC3
    elif implant_model in [ImplantModels.CI24RE, ImplantModels.CI500]:
        return ImplantCIC.CIC4
    else:
        return None


def check__rate_protocol(protocol=25.0):
    if protocol in [RateProtocol.SR5, RateProtocol.SR4, RateProtocol.HR5, RateProtocol.HR4]:
        return True
    else:
        return False


class PlatformStimulationMode(object):
    MP1 = 'MP1'
    MP2 = 'MP2'
    MP1_2 = 'MP1+2'
    BP = 'BP'


class MonopolarModes(object):
    MP1 = -1
    MP2 = -2
    MP1_2 = -3


class Platform(object):
    SP16 = 'SP16'


class SignalPath(object):
    left = 'left'
    right = 'right'


class PlatformParameters(object):
    def __init__(self, **kwargs):
        self.signal_path = kwargs.get('signal_path', SignalPath.left)
        self.flagged_electrodes = kwargs.get('flagged_electrodes', "")


class SR5(object):
    pw_min = 25.0
    pw_max = 434.4
    ipg_min = 7.0
    ipg_max = 58.0
    isg_min = 7.8
    fp_min = 64.8
    rate_max = 15432


class SR4(object):
    pw_min = 20.0
    pw_max = 429.4
    ipg_min = 5.6
    ipg_max = 56.6
    isg_min = 6.4
    fp_min = 52.0
    rate_max = 19230


class HR5(object):
    pw_min = 12.0
    pw_max = 12.0
    ipg_min = 6.0
    ipg_max = 6.0
    isg_min = 9.0
    fp_min = 39.0
    rate_max = 25641


class HR4(object):
    pw_min = 9.6
    pw_max = 9.6
    ipg_min = 4.8
    ipg_max = 4.8
    isg_min = 7.8                                    
    fp_min = 31.8
    rate_max = 31446


class TimeEvent(object):
    def __init__(self,
                 start=None,
                 end=None,
                 label='',
                 index=None,
                 sent=False):
        self.start = start
        self.end = end
        self.label = label
        self.index = index
        self.sent = sent
