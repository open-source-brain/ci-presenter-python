import numpy as np
from cochlear.nic import nic4
from cochlear_tools.stimulus.base_stimulus_definitions import SingleChannelSequence, ElectricParameters


class SymPulseTrain(SingleChannelSequence):
    def __init__(self, ep=ElectricParameters(), **kwargs):
        super(SymPulseTrain, self).__init__(ep=ep, **kwargs)
        self.duration = max(kwargs.setdefault('duration', 1.03), 0)  # in secs
        self.modulation_frequency = max(kwargs.setdefault('modulation_frequency', 20.0), 0)  # in Hz
        self.modulation_depth = max(kwargs.setdefault('modulation_depth', 0), 0)  # in current units
        self.modulation_depth_percentage = min(max(kwargs.setdefault('modulation_depth_percentage', 0.0), 0.0), 1.0)
        self.threshold = kwargs.setdefault('threshold', 0)
        self.modulation_phase = kwargs.setdefault('modulation_phase', 0)  # in rads
        self.rate = max(kwargs.setdefault('rate', 900.), 0)  # pulses per second
        self.amplitude = max(min(kwargs.setdefault('amplitude', 0.0), 255), 0)  # as determined during test
        self.anodic_first = bool(kwargs.setdefault('anodic_first', True))  # as determined during test
        self.alternating_polarity = bool(kwargs.setdefault('alternating_polarity', False))  # will alternate epochs polarity
        self.n_ephocs = int(max(kwargs.setdefault('n_ephocs', 1), 1))  # number of epochs to repeat sequence
        self.send_trigger = bool(kwargs.setdefault('send_trigger', True))
        self.mfl = ep.get_min_frame_length() * 1e-6
        _isg = ep.get_isg() * 1.0e-6
        self.rate = 1 / (_isg * round(np.maximum(self.mfl, 1. / self.rate) / _isg))
        self.power_up_interval = max(min(kwargs.setdefault('power_up_interval', 0.002), 0.013), 0.0)
        n = np.round(1 / (self.power_up_interval * self.rate))
        self.power_up_interval = 1. / (self.rate * n)
        self.power_up_interval = np.floor(self.power_up_interval / _isg) * _isg
        self.frame_length = 1 / self.rate
        self.duration = round(self.duration / self.frame_length) * self.frame_length
        self.ep = ep
        self.end_gap = kwargs.setdefault('end_gap', 0.0)
        if self.modulation_frequency:
            self.modulation_frequency = round(self.duration * self.modulation_frequency) / self.duration

    def _make_sequence(self, anodic_first):
        n_frames = int(self.duration * self.rate)
        modulation_frequency = self.modulation_frequency
        modulation_depth = self.modulation_depth
        modulation_phase = self.modulation_phase
        if self.threshold != 0:
            modulation_depth = max(
                min(self.amplitude, round((self.amplitude - self.threshold) * self.modulation_depth_percentage)), 0)

        frame_length = self.frame_length
        amplitude = self.amplitude
        ep = self.ep
        seq_null = nic4.Sequence()
        _inp = self.power_up_interval
        _stimulus_null = nic4.StimulusCommand(nic4.BiphasicStimulus(ep.el_active, ep.el_reference, 0, ep.pw,
                                                                    ep.ipg, _inp * 1e6))
        if self.frame_length > self.power_up_interval:
            frame_length = _inp
            n_power_up = np.floor(1 / (self.rate * self.power_up_interval)) - 1
            for _ in np.arange(n_power_up):
                seq_null.append(_stimulus_null)

        seq = nic4.Sequence()
        for _i in range(n_frames):
            _amplitude = modulation_depth * (1 - np.cos(2.0 * np.pi * modulation_frequency * self.frame_length * _i +
                                                        modulation_phase)) / 2 + (amplitude - modulation_depth)
            _amplitude = int(_amplitude)
            if anodic_first:
                _sym = nic4.BiphasicStimulus(ep.el_reference, ep.el_active, _amplitude, ep.pw, ep.ipg, frame_length * 1e6)
            else:
                _sym = nic4.BiphasicStimulus(ep.el_active, ep.el_reference, _amplitude, ep.pw, ep.ipg, frame_length * 1e6)
            _sym_cmd = nic4.StimulusCommand(_sym)
            if _i == 0 and self.send_trigger:
                _trigger_command = nic4.TriggerCommand(_sym)
                seq.append(_trigger_command)
                seq.append(seq_null)
            else:
                seq.append(_sym_cmd)
                seq.append(seq_null)
        if self.end_gap:
            self.end_gap = round(self.end_gap / self.power_up_interval) * self.power_up_interval
            self.end_gap = np.maximum(ep.get_isg() * 1e-6, self.end_gap)
            n_blocks = np.maximum(ep.get_isg(), round(self.end_gap/self.power_up_interval))
            for _ in np.arange(n_blocks):
                seq.append(_stimulus_null)
        return seq

    def get_sequence(self):
        seq1 = self._make_sequence(self.anodic_first)
        if self.alternating_polarity:
            seq2 = self._make_sequence(not self.anodic_first)
        out_seq = nic4.Sequence()
        for _i in range(self.n_ephocs):
            if self.alternating_polarity:
                if np.mod(_i, 2) == 0:
                    out_seq.append(seq1)
                else:
                    out_seq.append(seq2)
            else:
                out_seq.append(seq1)
        return out_seq


class QPPulseTrain(SingleChannelSequence):
    def __init__(self, ep=ElectricParameters(), **kwargs):
        super(QPPulseTrain, self).__init__(ep=ep, **kwargs)
        self.duration = max(kwargs.setdefault('duration', 1.03), 0)  # in secs
        self.modulation_frequency = max(kwargs.setdefault('modulation_frequency', 20.0), 0)  # in Hz
        self.modulation_depth = max(kwargs.setdefault('modulation_depth', 0), 0)  # in current units
        self.modulation_depth_percentage = min(max(kwargs.setdefault('modulation_depth_percentage', 0.0), 0.0), 1.0)
        self.threshold = kwargs.setdefault('threshold', 0)
        self.modulation_phase = kwargs.setdefault('modulation_phase', 0)  # in rads
        self.rate = max(kwargs.setdefault('rate', 900.), 0)  # pulses per second
        self.amplitude = max(min(kwargs.setdefault('amplitude', 0.0), 255), 0)  # as determined during test
        self.anodic_first = bool(kwargs.setdefault('anodic_first', True))  # as determined during test
        self.alternating_polarity = bool(kwargs.setdefault('alternating_polarity', False))  # will alternate epochs polarity
        self.n_ephocs = int(max(kwargs.setdefault('n_ephocs', 1), 1))  # number of epochs to repeat sequence
        self.send_trigger = bool(kwargs.setdefault('send_trigger', True))
        self.send_trigger = kwargs.setdefault('send_trigger', True)
        self.mfl = ep.get_min_frame_length() * 1e-6
        # as each shape consists of two pulses we round rate considering the 2 pulses
        _isg = ep.get_isg() * 1.0e-6
        self.rate = 1 / (_isg * round(np.maximum(2 * self.mfl, 1. / self.rate) / _isg))
        self.power_up_interval = max(min(kwargs.setdefault('power_up_interval', 0.002), 0.013), 0.0)
        n = np.round(1 / (self.power_up_interval * self.rate))
        self.power_up_interval = 1. / (self.rate * n)
        self.power_up_interval = np.floor(self.power_up_interval / _isg) * _isg
        self.frame_length = 1 / self.rate
        self.frame_length_1 = self.mfl
        self.frame_length_2 = self.frame_length - self.frame_length_1
        self.duration = round(self.duration / self.frame_length) * self.frame_length
        self.ep = ep
        self.end_gap = kwargs.setdefault('end_gap', 0.0)
        if self.modulation_frequency:
            self.modulation_frequency = round(self.duration * self.modulation_frequency) / self.duration

    def _make_sequence(self, anodic_first):
        n_frames = int(self.duration * self.rate)
        modulation_frequency = self.modulation_frequency
        modulation_depth = self.modulation_depth
        modulation_phase = self.modulation_phase
        if self.threshold != 0:
            modulation_depth = max(
                min(self.amplitude, round((self.amplitude - self.threshold) * self.modulation_depth_percentage)), 0)

        amplitude = self.amplitude
        ep = self.ep
        f_l_1 = self.frame_length_1
        f_l_2 = self.frame_length_2
        seq_null = nic4.Sequence()
        _inp = self.power_up_interval
        _stimulus_null = nic4.StimulusCommand(nic4.BiphasicStimulus(ep.el_active, ep.el_reference, 0, ep.pw,
                                                                    ep.ipg, _inp * 1e6))
        if self.frame_length_2 > self.power_up_interval:
            f_l_2 = _inp - f_l_1
            n_power_up = np.floor(1 / (self.rate * self.power_up_interval)) - 1
            for _ in np.arange(n_power_up):
                seq_null.append(_stimulus_null)

        seq = nic4.Sequence()
        for _i in range(n_frames):
            _amplitude = modulation_depth * (1 - np.cos(2.0 * np.pi * modulation_frequency * self.frame_length * _i +
                                                        modulation_phase)) / 2 + (amplitude - modulation_depth)
            _amplitude = int(_amplitude)
            if anodic_first:
                _sym_1 = nic4.BiphasicStimulus(ep.el_reference, ep.el_active, _amplitude, ep.pw, ep.ipg, f_l_1 * 1e6)
                _sym_2 = nic4.BiphasicStimulus(ep.el_active, ep.el_reference, _amplitude, ep.pw, ep.ipg, f_l_2 * 1e6)
            else:
                _sym_1 = nic4.BiphasicStimulus(ep.el_active, ep.el_reference, _amplitude, ep.pw, ep.ipg, f_l_1 * 1e6)
                _sym_2 = nic4.BiphasicStimulus(ep.el_reference, ep.el_active, _amplitude, ep.pw, ep.ipg, f_l_2 * 1e6)

            _sym_cmd_1 = nic4.StimulusCommand(_sym_1)
            _sym_cmd_2 = nic4.StimulusCommand(_sym_2)
            if _i == 0 and self.send_trigger:
                _trigger_command = nic4.TriggerCommand(_sym_1)
                seq.append(_trigger_command)
                seq.append(_sym_cmd_2)
                seq.append(seq_null)
            else:
                seq.append(_sym_cmd_1)
                seq.append(_sym_cmd_2)
                seq.append(seq_null)

        if self.end_gap:
            self.end_gap = round(self.end_gap / self.power_up_interval) * self.power_up_interval
            self.end_gap = np.maximum(ep.get_isg() * 1e-6, self.end_gap)
            n_blocks = np.maximum(ep.get_isg(), round(self.end_gap/self.power_up_interval))
            for _ in np.arange(n_blocks):
                seq.append(_stimulus_null)
        return seq

    def get_sequence(self):
        seq1 = self._make_sequence(self.anodic_first)
        if self.alternating_polarity:
            seq2 = self._make_sequence(not self.anodic_first)
        out_seq = nic4.Sequence()
        for _i in range(self.n_ephocs):
            if self.alternating_polarity:
                if np.mod(_i, 2) == 0:
                    out_seq.append(seq1)
                else:
                    out_seq.append(seq2)
            else:
                out_seq.append(seq1)
        return out_seq


class SymPulseTrainBursts(SingleChannelSequence):
    def __init__(self, ep=ElectricParameters(), **kwargs):
        super(SymPulseTrainBursts, self).__init__(ep=ep, **kwargs)
        self.duration = max(kwargs.setdefault('duration', 1.03), 0)  # in secs
        self.modulation_frequency = max(kwargs.setdefault('modulation_frequency', 20.0), 0)  # in Hz
        self.modulation_depth = max(kwargs.setdefault('modulation_depth', 0), 0)  # in current units
        self.modulation_depth_percentage = min(max(kwargs.setdefault('modulation_depth_percentage', 0.0), 0.0), 1.0)
        self.threshold = kwargs.setdefault('threshold', 0)
        self.modulation_phase = kwargs.setdefault('modulation_phase', 0)  # in rads
        self.rate = max(kwargs.setdefault('rate', 900.), 0)  # pulses per second
        self.amplitude = max(min(kwargs.setdefault('amplitude', 0.0), 255), 0)  # as determined during test
        self.anodic_first = bool(kwargs.setdefault('anodic_first', True))  # as determined during test
        self.alternating_polarity = bool(kwargs.setdefault('alternating_polarity', False))  # will alternate epochs polarity
        self.n_ephocs = int(max(kwargs.setdefault('n_ephocs', 1), 1))  # number of epochs to repeat sequence
        self.send_trigger = bool(kwargs.setdefault('send_trigger', True))
        self.burst_duration = max(kwargs.setdefault('burst_duration', .05), 0)  # in secs
        self.burst_rate = max(kwargs.setdefault('burst_rate', 2.), 0)  # in Hz
        self.mfl = ep.get_min_frame_length() * 1e-6
        _isg = ep.get_isg() * 1.0e-6
        self.rate = 1 / (_isg * round(np.maximum(self.mfl, 1. / self.rate) / _isg))
        self.power_up_interval = max(min(kwargs.setdefault('power_up_interval', 0.002), 0.013), 0.0)
        n = np.round(1 / (self.power_up_interval * self.rate))
        self.power_up_interval = 1. / (self.rate * n)
        self.power_up_interval = np.floor(self.power_up_interval / _isg) * _isg
        self.frame_length = 1 / self.rate
        _burst_period = 1. / self.burst_rate
        _burst_period = round(_burst_period / self.frame_length) * self.frame_length
        self.burst_rate = 1. / _burst_period
        self.duration = round(self.duration / _burst_period) * _burst_period
        self.burst_duration = np.maximum(round(self.burst_duration / self.frame_length) * self.frame_length,
                                         self.frame_length)
        self.ep = ep
        self.end_gap = kwargs.setdefault('end_gap', 0.0)
        if self.modulation_frequency:
            self.modulation_frequency = round(_burst_period * self.modulation_frequency) / _burst_period
            self.burst_duration = np.minimum(
                np.maximum(round(self.burst_duration * self.modulation_frequency) / self.modulation_frequency,
                                             1 / self.modulation_frequency),
                _burst_period)

    def _make_sequence(self, anodic_first):
        modulation_frequency = self.modulation_frequency
        modulation_depth = self.modulation_depth
        modulation_phase = self.modulation_phase
        if self.threshold != 0:
            modulation_depth = max(
                min(self.amplitude, round((self.amplitude - self.threshold) * self.modulation_depth_percentage)), 0)

        frame_length = self.frame_length
        amplitude = self.amplitude
        ep = self.ep
        n_frames_per_block = int(self.burst_duration / frame_length)
        n_blocks = self.duration * self.burst_rate
        _inp = self.power_up_interval
        _stimulus_null = nic4.StimulusCommand(nic4.BiphasicStimulus(ep.el_active, ep.el_reference, 0, ep.pw,
                                                                    ep.ipg, _inp * 1e6))
        seq_null = nic4.Sequence()
        if self.frame_length > self.power_up_interval:
            _inp = self.power_up_interval
            frame_length = _inp
            n_power_up = np.floor(1 / (self.rate * self.power_up_interval)) - 1
            for _ in np.arange(n_power_up):
                seq_null.append(_stimulus_null)
        seq = nic4.Sequence()
        for _i_b in np.arange(n_blocks):
            for _i_f in range(n_frames_per_block):
                _amplitude = modulation_depth * (1 - np.cos(2.0 * np.pi * modulation_frequency * self.frame_length * _i_f +
                                                            modulation_phase)) / 2 + (amplitude - modulation_depth)
                _amplitude = int(_amplitude)
                if anodic_first:
                    _sym = nic4.BiphasicStimulus(ep.el_reference, ep.el_active, _amplitude, ep.pw, ep.ipg,
                                                 frame_length * 1e6)
                else:
                    _sym = nic4.BiphasicStimulus(ep.el_active, ep.el_reference, _amplitude, ep.pw, ep.ipg,
                                                 frame_length * 1e6)
                _sym_cmd = nic4.StimulusCommand(_sym)
                if _i_f == 0 and _i_b == 0 and self.send_trigger:
                    _trigger_command = nic4.TriggerCommand(_sym)
                    seq.append(_trigger_command)
                    seq.append(seq_null)
                else:
                    seq.append(_sym_cmd)
                    seq.append(seq_null)

                if _i_f == n_frames_per_block - 1:
                    _gap_dur = round(_gap_dur / self.power_up_interval) * self.power_up_interval
                    n_blocks = np.maximum(ep.get_isg(), round(self._gap_dur / self.power_up_interval))
                    for _ in np.arange(n_blocks):
                        seq.append(_stimulus_null)

        if self.end_gap:
            self.end_gap = round(self.end_gap / self.power_up_interval) * self.power_up_interval
            self.end_gap = np.maximum(ep.get_isg() * 1e-6, self.end_gap)
            n_blocks = np.maximum(ep.get_isg(), round(self.end_gap/self.power_up_interval))
            for _ in np.arange(n_blocks):
                seq.append(_stimulus_null)
        return seq

    def get_sequence(self):
        seq1 = self._make_sequence(self.anodic_first)
        if self.alternating_polarity:
            seq2 = self._make_sequence(not self.anodic_first)
        out_seq = nic4.Sequence()
        for _i in range(self.n_ephocs):
            if self.alternating_polarity:
                if np.mod(_i, 2) == 0:
                    out_seq.append(seq1)
                else:
                    out_seq.append(seq2)
            else:
                out_seq.append(seq1)
        return out_seq


class SequencedStimuli(SingleChannelSequence):
    def __init__(self, sequence=None, time_events=None, **kwargs):
        super(SequencedStimuli, self).__init__()
        self.n_ephocs = int(kwargs.setdefault('n_ephocs', 1))  # number of epochs to repeat sequence
        self._sequence = sequence
        self._time_events = time_events
        self.duration = None

    def _make_sequence(self):
        pass

    def get_sequence(self):
        return self._sequence


class CustomSequence(SingleChannelSequence):
    def __init__(self, ep=ElectricParameters(), sequence=nic4.Sequence(), **kwargs):
        super(CustomSequence, self).__init__(ep=ep, **kwargs)
        self.sequence = sequence
        self.ep = ep

    def _make_sequence(self): pass

    def get_sequence(self):
        out_seq = self.sequence
        return out_seq


def looper(stimulus=SingleChannelSequence, n_repetitions=1):
    out_seq = nic4.Sequence()
    _seq = stimulus.get_sequence()
    _ep = stimulus.ep
    for _i in range(n_repetitions):
        out_seq.append(_seq)
    _out = CustomSequence(ep=_ep, sequence=out_seq)
    return _out

