from cochlear_tools.definitions import *
from cochlear.nic import nic4
import numpy as np
from abc import ABCMeta, abstractmethod


class ElectricParameters(object):
    def __init__(self, **kwargs):
        self.__el_reference = None
        self.__el_active = None
        self.__implant = None
        self.pw = kwargs.get('pw', SR5.pw_min)
        self.ipg = kwargs.get('ipg', SR5.ipg_min)
        self.pw_min = kwargs.get('pw_min', SR5.pw_min)
        self.implant = kwargs.get('implant', ImplantCIC.CIC4)
        self.el_reference = kwargs.get('el_reference', MonopolarModes.MP1_2)
        self.el_active = kwargs.get('el_active', None)
        self.rate_protocol = kwargs.get('rate_protocol', SR5)

    def get_el_active(self):
        return self._el_active

    def set_el_active(self, value):
        if value is not None:
            value = max(min(value, 22), -3)
            self._el_active = int(value)

    el_active = property(get_el_active, set_el_active)

    def get_el_reference(self):
        return self._el_reference

    def set_el_reference(self, value):
        value = max(min(value, 22), -3)
        self._el_reference = int(value)

    el_reference = property(get_el_reference, set_el_reference)

    def get_min_pulse_width_us(self):
            return self.rate_protocol.pw_min

    min_pulse_width_us = property(get_min_pulse_width_us)

    def get_implant_cic(self):
        return self.__implant

    def set_implant_cic(self, value):
        self.__implant = value

    implant = property(get_implant_cic, set_implant_cic)

    def get_isg(self):
        return self.rate_protocol.isg_min

    def get_min_frame_length(self):
        return max(self.rate_protocol.fp_min, 2 * self.pw + self.ipg + self.rate_protocol.isg_min)

    def current_level_to_current(self, value):
        out = None
        if self.implant == ImplantCIC.CIC3:
            out = 10. * np.exp(value * np.log(175.) / 255.)
        if self.implant == ImplantCIC.CIC4:
            out = 17.5 * 100. ** (value / 255.)
        return out

    def get_parameters(self):
        out = {'pw': self.pw,
               'ipg': self.ipg,
               'el_active': self.el_active,
               'el_reference': self.el_reference,
               'implant': self.implant,
               'isg_min': self.rate_protocol.isg_min
               }
        return out

    def current_to_current_level(self, value):
        out = None
        if self.implant == ImplantCIC.CIC3:
            out = int(round(np.log(value / 10) * 255. / np.log(175.)))
        if self.implant == ImplantCIC.CIC4:
            out = int(round(np.log10(value / 17.5) * 255. / 2))
        return out


class SingleChannelSequence(object):
    __metaclass__ = ABCMeta

    def __init__(self, ep=ElectricParameters(), **kwargs):
        self.duration = None
        self.n_epochs = None
        self.ep = ep
        self.frame_length = 0

    def get_parameters(self):
        _params = self.__dict__
        out = {}
        for _p in _params.keys():
            if isinstance(_params[_p], int) or isinstance(_params[_p], float) or isinstance(_params[_p], bool):
                out[_p] = _params[_p]
        return out

    @abstractmethod
    def _make_sequence(self, anodic_first): pass

    @abstractmethod
    def get_sequence(self): pass


class Default(SingleChannelSequence):
    def __init__(self, ep=ElectricParameters(), **kwargs):
        super(Default, self).__init__(ep=ep, **kwargs)
        self.frame_length = 0.0

    def _make_sequence(self): pass

    def get_sequence(self):
        out_seq = nic4.Sequence()
        return out_seq


class BilateralSequence(object):
    def __init__(self, left_channel_sequence=SingleChannelSequence,
                 right_channel_sequence=SingleChannelSequence,
                 time_events: [TimeEvent] = []):
        self.left_channel_sequence = left_channel_sequence
        self.right_channel_sequence = right_channel_sequence
        self.time_events = time_events

    def get_stimulus(self):
        return self.left_channel_sequence, self.right_channel_sequence

