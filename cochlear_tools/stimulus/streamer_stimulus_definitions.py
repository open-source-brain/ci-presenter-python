from cochlear_tools.stimulus.am_pulse_trains import *
from cochlear_tools.stimulus.base_stimulus_definitions import ElectricParameters, Default, SignalPath
from cochlear_tools.stimulus.base_stimulus_definitions import BilateralSequence
from abc import ABCMeta, abstractmethod


class StreamerStimulus(object):
    __metaclass__ = ABCMeta

    def __init__(self, **kwargs):
        pass

    @abstractmethod
    def get_sequence(self) -> BilateralSequence: pass


class CIStimulusMaker(StreamerStimulus):

    def __init__(self, electric_parameters={}, function_name='Default', defaults={}, path='left', label=''):
        """
        This class will put together the specific CI information passed by electric_paremeters and will apply those to
        a sequence maker passed in the function_name.
        The specific CI information is taken from the path parameter "left" or "right" which indicate from which CI the
        electrical parameters will be taken
        :param electric_parameters: specific implant parameters specifications dictionary
        :param function_name: name of function that will be called to generate stimulus
        :param defaults: default stimulus parameters
        :param path: either "left" or "right" implant
        :param label: a label specified by user
        """
        super(CIStimulusMaker, self).__init__()
        self._electrical_parameters = ElectricParameters(**electric_parameters)
        self.type = function_name
        self.defaults = defaults
        self.current_parameters = defaults
        self.path = path
        self.label = label
        self.step_size = 1

    def get_sequence(self, updated_values={}):
        _parameters = self.current_parameters.copy()
        for _key in updated_values.keys():
            if _key in _parameters.keys():
                _parameters[_key] = updated_values[_key]

        _fun = globals()[self.type]
        _stim = _fun(self.electrical_parameters, **_parameters)
        self.current_parameters = _stim.get_parameters()
        left_seq = CustomSequence(ep=self._electrical_parameters, sequence=Default().get_sequence())
        right_seq = CustomSequence(ep=self._electrical_parameters, sequence=Default().get_sequence())
        if self.path == SignalPath.left:
            left_seq = CustomSequence(ep=self._electrical_parameters, sequence=_stim.get_sequence())
        if self.path == SignalPath.right:
            right_seq = CustomSequence(ep=self._electrical_parameters, sequence=_stim.get_sequence())

        out_seq = BilateralSequence(left_channel_sequence=left_seq, right_channel_sequence=right_seq)
        return out_seq

    def set_electrical_parameters(self, value):
        self._electrical_parameters = ElectricParameters(**value)

    def get_electrical_parameters(self):
        return self._electrical_parameters

    electrical_parameters = property(get_electrical_parameters, set_electrical_parameters)


class JoinStimulus(StreamerStimulus):
    def __init__(self, function_name='', stimuli=[CIStimulusMaker], defaults={}, label=''):
        super(JoinStimulus, self).__init__()
        self.type = function_name
        self.defaults = defaults
        self.stimuli = stimuli
        self.current_parameters = defaults
        self.label = label
        self.step_size = 1
        self.path = None

    def get_sequence(self, updated_values={}):
        _parameters = self.current_parameters.copy()
        for _key in updated_values.keys():
            if _key in _parameters.keys():
                _parameters[_key] = updated_values[_key]
        _fun = globals()[self.type]
        _stim = _fun(stimuli=self.stimuli, **_parameters)
        self.current_parameters = _parameters
        return _stim.get_sequence()

    def set_electrical_parameters(self, value):
        print('')

    def get_electrical_parameters(self):
        return self.stimuli[0].electrical_parameters

    electrical_parameters = property(get_electrical_parameters, set_electrical_parameters)


class SeqJoiner(StreamerStimulus):
    def __init__(self, stimuli: [CIStimulusMaker] = [], **kwargs):
        super(SeqJoiner, self).__init__()
        self.n_epochs = int(kwargs.setdefault('n_epochs', 1))  # number of epochs to repeat sequence
        self.sequential = kwargs.setdefault('sequential', True)  # binaural simuli will be concatenated sequentially if this is true
        self.stimuli = stimuli
        self.duration = None

    def _make_sequence(self):
        seq_left = nic4.Sequence()
        seq_right = nic4.Sequence()
        ep_left = ElectricParameters()
        ep_right = ElectricParameters()
        for _i, _stim in enumerate(self.stimuli):
            _out_sequence = _stim.get_sequence()
            _stim_left, _stim_right = _out_sequence.get_stimulus()
            if self.sequential:
                _duration_l = _stim_left.get_sequence().getStimulationTime_ms()
                _duration_r = _stim_right.get_sequence().getStimulationTime_ms()
                _max_duration = max(_duration_l, _duration_r)
                f_l = _stim_left.frame_length
                f_r = _stim_right.frame_length
                f_l = f_r if f_l == 0 else f_l
                f_r = f_l if f_r == 0 else f_r
                _empty_seq_l = self.make_null_stimulus(frame_length=f_l*1e6,
                                                       duration_ms=_max_duration - _duration_l)
                _empty_seq_r = self.make_null_stimulus(frame_length=f_r*1e6,
                                                       duration_ms=_max_duration - _duration_r)
                seq_left.append(_stim_left.get_sequence())
                seq_right.append(_stim_right.get_sequence())
                seq_left.append(_empty_seq_l)
                seq_right.append(_empty_seq_r)
            else:
                seq_left.append(_stim_left.get_sequence())
                seq_right.append(_stim_right.get_sequence())
            ep_left = _stim_left.ep
            ep_right = _stim_right.ep

        stim_left = CustomSequence(ep=ep_left, sequence=seq_left)
        stim_right = CustomSequence(ep=ep_right, sequence=seq_right)
        out_seq = BilateralSequence(left_channel_sequence=stim_left,
                                    right_channel_sequence=stim_right)

        return out_seq

    def get_sequence(self):
        current_seq = self._make_sequence()
        stim_left, stim_right = current_seq.get_stimulus()
        out_seq_left = looper(stimulus=stim_left, n_repetitions=self.n_epochs)
        out_seq_right = looper(stimulus=stim_right, n_repetitions=self.n_epochs)
        out_seq = BilateralSequence(left_channel_sequence=out_seq_left,
                                    right_channel_sequence=out_seq_right)
        return out_seq

    @staticmethod
    def make_null_stimulus(frame_length=0.0, duration_ms=0.0):
        empty_seq = nic4.Sequence()
        if duration_ms > 0 and frame_length > 0:
            stim_null = nic4.NullStimulus(frame_length)
            command_null = nic4.StimulusCommand(stim_null)
            empty_seq = nic4.Sequence(int(duration_ms * 1e3 / frame_length))
            empty_seq.append(command_null)
        return empty_seq


class CustomStreamerStimulus(StreamerStimulus):
    def __init__(self, sequence: BilateralSequence = None):
        super(CustomStreamerStimulus, self).__init__()
        self._sequence = sequence

    def get_sequence(self):
        return self._sequence
