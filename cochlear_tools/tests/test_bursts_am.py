from cochlear_tools.stimulus.base_stimulus_definitions import ElectricParameters
from cochlear_tools.stimulus.streamer_stimulus_definitions import CIStimulusMaker
from cochlear_tools.streamer import get_streamer, play_stimulus

ep_1 = {"el_active": 16, "el_reference": -3, "pw": 43.0}
ep = ElectricParameters(**ep_1)

_parameters = {}
_parameters['electric_parameters'] = ep
_parameters['amplitude'] = 255
_parameters['thr'] = 0
_parameters['modulation_depth'] = 0
_parameters['anodic_first'] = True
_parameters['n_ephocs'] = 10
_parameters['alternating_polarity'] = True
_parameters['end_gap'] = 0.1

seq = CIStimulusMaker(electric_parameters=ep_1, function_name='SymPulseTrainBursts',
                      defaults=_parameters)
streamer = get_streamer(ep=ep)
play_stimulus(streamer, seq)
streamer.waitUntilFinished()  # Wait for stream to complete
streamer.stop()				  # Stop the streamer
