from cochlear_tools.stimulus.base_stimulus_definitions import ElectricParameters
from cochlear_tools.stimulus.streamer_stimulus_definitions import SeqJoiner, CIStimulusMaker
from cochlear_tools.streamer import get_streamer, play_stimulus
import numpy as np

ep_1 = {"el_active": 16, "el_reference": -3, "pw": 43.0}
_parameters_1 = {}
_parameters_1['electric_parameters'] = ep_1
_parameters_1['amplitude'] = 144
_parameters_1['modulation_depth'] = 0
_parameters_1['anodic_first'] = False
_parameters_1['n_ephocs'] = 1
_parameters_1['alternating_polarity'] = False
_parameters_1['end_gap'] = 0
_parameters_1['send_trigger'] = True
_parameters_1['duration'] = 0.150
_parameters_1['rate'] = 125
_parameters_1['modulation_frequency'] = 20

seq_1 = CIStimulusMaker(electric_parameters=ep_1, function_name='QPPulseTrain',
                        defaults=_parameters_1)

ep_2 = {"el_active": 16, "el_reference": -3, "pw": 43.0}
_parameters_2 = {}
_parameters_2['electric_parameters'] = ep_2
_parameters_2['amplitude'] = 149
_parameters_2['modulation_depth'] = 47
_parameters_2['modulation_phase'] = np.pi
_parameters_2['anodic_first'] = False
_parameters_2['n_ephocs'] = 1
_parameters_2['alternating_polarity'] = False
_parameters_2['end_gap'] = 0
_parameters_2['send_trigger'] = False
_parameters_2['duration'] = 0.150
_parameters_2['rate'] = 125
_parameters_2['modulation_frequency'] = 20

seq_2 = CIStimulusMaker(electric_parameters=ep_2, function_name='QPPulseTrain',
                        defaults=_parameters_2)


seq_left = SeqJoiner(stimuli=[seq_1, seq_2], n_ephocs=10).get_sequence()
seq_right = SeqJoiner(stimuli=[seq_1, seq_2], n_ephocs=10).get_sequence()
streamer_left = get_streamer(ep=ElectricParameters(**ep_1), signal_path='left')
streamer_right = get_streamer(ep=ElectricParameters(**ep_2), signal_path='right')
play_stimulus(streamer_left, seq_left, streamer_right)
streamer_left.waitUntilFinished()  # Wait for stream to complete
streamer_left.stop()				  # Stop the streamer
