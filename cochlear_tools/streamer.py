from cochlear.nic import nic4
import time
import sys
from cochlear_tools.definitions import *
from cochlear_tools.stimulus.base_stimulus_definitions import ElectricParameters
from cochlear_tools.stimulus.streamer_stimulus_definitions import StreamerStimulus
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import pyqtSignal
import numpy as np
from cochlear_tools.io import plot
import os
latency_ms = 200
global OFF_LINE, GENERATE_PLOTS
OFF_LINE = False
GENERATE_PLOTS = False
LOG_STREAM_DIR = os.path.expanduser('~') + os.path.sep + '.pygenerator'
LOG_STREAM_PATH_R = LOG_STREAM_DIR + os.path.sep + 'stream_right.txt'
LOG_STREAM_PATH_L = LOG_STREAM_DIR + os.path.sep + 'stream_left.txt'
if not os.path.isdir(LOG_STREAM_DIR):
    os.mkdir(LOG_STREAM_DIR)


def get_streamer(platform='SP16', ep=ElectricParameters(), signal_path='left'):
    # Configuration:
    p = nic4.Properties()
    p.add("platform", str(platform))
    p.add("implant", ep.implant)
    if ep.el_reference == -1:
        p.add("mode", PlatformStimulationMode.MP1)
    elif ep.el_reference == -2:
        p.add("mode", PlatformStimulationMode.MP2)
    elif ep.el_reference == -3:
        p.add("mode", PlatformStimulationMode.MP1_2)
    else:
        p.add("mode", PlatformStimulationMode.BP)

    p.add("flagged_electrodes", "")
    p.add("min_pulse_width_us", str(ep.min_pulse_width_us))
    if OFF_LINE:
        p.add("go_live", "off")
    else:
        p.add("go_live", "on")
    p.add("auto_pufs", "off")
    if signal_path == 'left':
        p.add("log_stream_filename", LOG_STREAM_PATH_L)
    if signal_path == 'right':
        p.add("log_stream_filename", LOG_STREAM_PATH_R)
    # p.add("signal_path", str(signal_path))
    p.add("latency_ms", str(latency_ms))
    p.add("c_levels_pulse_width_us", str(ep.pw))
    p.add("c_levels", "255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255")
    p.add("log_stream_format", "basic")
    # Actual streaming:
    streamer = nic4.Streamer(p)  # The configuration is passed in as argument
    return streamer


def play_stimulus(nic_streamer_left=nic4.Streamer,
                  stimulus_sequence_left=nic4.Sequence(),
                  nic_streamer_right=nic4.Streamer,
                  stimulus_sequence_right=nic4.Sequence(),
                  ):
    # 1 sec power up pulses
    stim_null = nic4.NullStimulus(1000.0)
    command_null = nic4.StimulusCommand(stim_null)
    seqPufs = nic4.Sequence(1000)
    seqPufs.append(command_null)

    # definition of Sequence power up frames
    ini_seq = nic4.Sequence()
    ini_seq.append(seqPufs)
    # Actual streaming:
    nic_streamer_left.start()  # Streamer is started here
    nic_streamer_right.start()
    nic_streamer_left.sendData(seqPufs, True)
    nic_streamer_left.sendData(stimulus_sequence_left)
    nic_streamer_right.sendData(seqPufs, True)
    nic_streamer_right.sendData(stimulus_sequence_right)
    

class StimulusVariable(object):
    def __init__(self, value=None, min_val=None, max_val=None):
        self._value = value
        self.min_val = min_val
        self.max_val = max_val

    def set_value(self, value):
        if isinstance(value, float) or isinstance(value, int):
            self._value = min(max(value, self.min_val),self.max_val)
        assert isinstance(value, self.type)
        if isinstance(value, bool):
            self._value = bool(value)

    def get_value(self):
        return self._value

    def __repr__(self):
        return self._value


class Player(QtCore.QObject):
    signal_stream_ended = pyqtSignal(name="signal_stream_ended")
    signal_time_event = pyqtSignal(object, name="signal_time_event")

    def __init__(self):
        super(Player, self).__init__()
        self.platform = None
        self.off_line = False
        self._streamer_left = None
        self._streamer_right = None
        self.__stop_streaming = False

    def initialize_streamer(self, left_electrical_parameters=ElectricParameters(),
                            right_electrical_parameters=ElectricParameters()):
        self._streamer_left = get_streamer(platform=self.platform,
                                           ep=left_electrical_parameters,
                                           signal_path="left")
        self._streamer_right = get_streamer(platform=self.platform,
                                            ep=right_electrical_parameters,
                                            signal_path="right")

    def play(self, sequence: StreamerStimulus = None, time_events=[]):
        """

        :param sequence: StreamerStimulus class
        :param time_events: list of TimeEvent items
        :return:
        """
        try:
            seq = sequence.get_sequence()
            sequence_left, sequence_right = seq.get_stimulus()

            self.initialize_streamer(left_electrical_parameters=sequence_left.ep,
                                     right_electrical_parameters=sequence_right.ep)

            self.__stop_streaming = False
            play_stimulus(self._streamer_left, sequence_left.get_sequence(),
                          self._streamer_right, sequence_right.get_sequence())
            _status_left = self._streamer_left.streamStatus()
            _status_right = self._streamer_right.streamStatus()
            count = 0
            _time_pause = 0.01
            _time_out = 5
            _time_out_reached = False
            _update_time = 1.0
            _time_counter = 0.0
            all_event_sent = False
            ini_time = time.time()
            while (not self._streamer_left.isFinished() or not self._streamer_right.isFinished()) or \
                    (OFF_LINE and not all_event_sent):
                if self.__stop_streaming:
                    self._streamer_left.stop()
                    self._streamer_right.stop()
                    break
                time.sleep(_time_pause)

                QtGui.QApplication.processEvents()

                if OFF_LINE:
                    _none_event_sent = np.all([not _event.sent for _event in time_events])
                    all_event_sent = np.all([_event.sent for _event in time_events])
                _time = time.time()
                _delta = (_time - ini_time) * 1000.0 - 1000.0 - latency_ms

                # print(_time, _delta)
                for _event in time_events:
                    if _event.end > _delta > _event.start:
                        self.signal_time_event.emit(_event)
                    elif _event.end < _delta and not _event.sent:
                        _event.sent = True
                        self.signal_time_event.emit(_event)

                if _delta > _time_counter:
                    _time_counter += _update_time
                    print(_status_left, _status_right)
                if count * _time_pause > _time_out:
                    print('streaming timeout ({:.1f} s) reached'.format(_time_out))
                    _time_out_reached = True
                    break
                count += 1
            if not _time_out_reached:
                self._streamer_left.stop()
                self._streamer_right.stop()
                self.signal_stream_ended.emit()
                if GENERATE_PLOTS:
                    plot.read()
        except IOError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))
        except ValueError:
            print("Could not convert data to an integer.")
        except:
            print("Unexpected error:", sys.exc_info()[0])

    def stop(self):
        print('stoping')
        self.__stop_streaming = True

    def on_mode_changed(self, value):
        self.off_line = value
