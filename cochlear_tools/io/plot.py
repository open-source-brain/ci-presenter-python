import pyqtgraph as pg
import pandas as pd
from PyQt5 import QtGui
from PyQt5.QtGui import QPen, QColor
import sys
import numpy as np
import os
pg.setConfigOption('leftButtonPan', False)
LOG_STREAM_DIR = os.path.expanduser('~') + os.path.sep + '.pygenerator'
LOG_STREAM_PATH_R = LOG_STREAM_DIR + os.path.sep + 'stream_right.txt'
LOG_STREAM_PATH_L = LOG_STREAM_DIR + os.path.sep + 'stream_left.txt'
LOG_STREAM_PATH_R_TEMP = LOG_STREAM_DIR + os.path.sep + 'stream_right_temp.txt'
LOG_STREAM_PATH_L_TEMP = LOG_STREAM_DIR + os.path.sep + 'stream_left_temp.txt'


def rename(path='', new_path=''):
    with open(path, 'r') as infile, \
         open(new_path, 'w') as outfile:
        data = infile.read()
        data = data.replace("#", " ")
        outfile.write(data)


def plot_pulses(df=pd.DataFrame, plotWidget=pg.plot, color='b'):
    if not df.size:
        return
    df = df.assign(ae_re=df.apply(lambda xx: [xx['ae'], xx['re']], axis=1))
    df['ae_re'] = df['ae_re'].apply(lambda xx: '_'.join(map(str, xx)))
    df = df.sort_values(by='ae_re')
    _sub_set = df.groupby(['ae_re', 'info'])
    for _group, _df in _sub_set:
        _type = _df['info'].unique()
        ae = np.atleast_2d(_df.ae).T
        re = np.atleast_2d(_df.re).T
        pw_1 = _df.pw1_us.values
        pw_2 = _df.pw2_us.values
        ipg = _df.ipg_us.values
        time_offset = _df.runtime_s.values
        amp = _df.cl.values
        amp = amp / 256.0 / 2
        x0 = time_offset
        x1 = x0 + pw_1 * 1e-6
        x2 = x1 + ipg * 1e-6
        x3 = x2 + pw_2 * 1e-6
        x = np.array([x0, x0, x1, x1, x2, x2, x3, x3]).T
        if _type == "TRIGGER":
            amp[:] = 1
            y = np.array([np.zeros(amp.shape), amp, amp, np.zeros(amp.shape), np.zeros(amp.shape), 0*amp,
                          0*amp, np.zeros(amp.shape)]).T
        else:
            y = np.array([np.zeros(amp.shape), amp, amp, np.zeros(amp.shape), np.zeros(amp.shape), -amp,
                          -amp, np.zeros(amp.shape)]).T
            y[np.where(ae > 22)[0]] *= -1
        if np.any(ae[ae > 22]):
            ae[ae > 22] = re[ae > 22]
        _offset_idx = np.where(np.logical_and(0 < ae, ae <= 22))[0]
        y += ae
        _x, _y = np.squeeze(np.reshape(x, (-1, 1))), np.squeeze(np.reshape(y, (-1, 1)))
        if _type == "RF":
            plotWidget.plot(_x, _y, pen={'color': color})
        elif _type == "TRIGGER":
            plotWidget.plot(_x, _y, pen={'color': "y"})
        else:
            plotWidget.plot(_x, _y, pen={'color': QColor(200, 200, 200)})
    plotWidget.setLabel('bottom', "Time [sec]")
    plotWidget.setLabel('left', "Electrode")
    pg.QtGui.QApplication.processEvents()


def read():
    path_l = LOG_STREAM_PATH_L
    path_r = LOG_STREAM_PATH_R
    path_l_temp = LOG_STREAM_PATH_L_TEMP
    path_r_temp = LOG_STREAM_PATH_R_TEMP

    # we read the data and remove no RF data or "#" character
    with open(path_l) as f, open(path_l_temp, mode="w+") as out:
        for line in f:
            if line.find("NO_RF") is not -1:
                continue
            out.write(line.replace("#", ""))

    with open(path_r) as f, open(path_r_temp, mode="w+") as out:
        for line in f:
            if line.find("NO_RF") is not -1:
                continue
            out.write(line.replace("#", ""))

    df_l = pd.read_csv(path_l_temp, header=0, sep='\s+')
    df_r = pd.read_csv(path_r_temp, header=0, sep='\s+')
    plotWidget = pg.plot(title="CI stimulation output")

    _df = df_l.query('ae > 0')
    plot_pulses(_df, plotWidget=plotWidget, color='b')

    # #  power up pulses
    # _df = df_l.query('ae == 0')
    # plot_pulses(_df, plotWidget=plotWidget, color='b')
    # # right channel
    _df = df_r.query('ae > 0')
    plot_pulses(_df, plotWidget=plotWidget, color='r')

    # #  power up pulses
    # _df = df_r.query('ae == 0')
    # plot_pulses(_df, plotWidget=plotWidget, color='r')


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    app.setStyle(QtGui.QStyleFactory.create('plastique'))
    read()
    sys.exit(app.exec_())


